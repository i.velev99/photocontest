## **PhotoContest**

PhotoContest is web application that enables your users to enroll contests, win points and ranks, also, rate other photos. Organizers can create contests and rate pictures. 

## 
  - Visible without authentication
    - the application start page
    - the user login form
    - the user registration form
    - the contest about page
    - the rank system page
    
  - When authenticated, user can upload photo in a contest and rate other photos, if it is jury. Registered users can modify their personal information as well.

  - The app have profile page that shows user’s  first name, last name.
 
  - The dashboard page shows contests, which can be filtered by phase.

### Technologies
- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *Thymeleaf* - as a template engine in order to load a big amount of data (beers, users). Also thymeleaf allows inserting and replacing views and imports, which makes the code way more readable and replaceable
- *MariaDB* - as database
- *Hibernate* - access the database
- *Spring Security* - for managing users, roles and authentication
- *Git* - for source control management and documentation / manual of the application

### Screenshots

![PCDB](/uploads/ec87cf62825508e2b1cf9c3cb3ecd03b/PCDB.png)
