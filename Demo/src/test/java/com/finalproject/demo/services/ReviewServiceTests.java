package com.finalproject.demo.services;

import com.finalproject.demo.Helpers;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.repositories.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTests {

    @Mock
    private ReviewRepository reviewRepository;

    @InjectMocks
    private ReviewServiceImpl reviewService;

    @Test
    public void getAll_Should_CallRepository() {
        reviewService.getAll();
        Mockito.verify(reviewRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnReview_When_MatchExists() {

        //Arrange
        Mockito.when(reviewRepository.getById(1))
                .thenReturn(Helpers.createMockReview());

        //Act
        Review result = reviewService.getById(1);
        User user = Helpers.createMockUserOrganizer();

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Nice photo", result.getReview());
        Assertions.assertEquals(5, result.getPoints());
        Assertions.assertEquals(user, result.getCreatedBy());
    }

    @Test
    public void create_Should_Throw_When_PhaseIsNotSecond() {
        //Arrange
        Review review = Helpers.createMockReview();

        //Act, Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> reviewService.create(review, review.getCreatedBy()));
    }

    @Test
    public void create_Should_Throw_When_UserIsNotJury() {
        //Arrange
        Review review = Helpers.createMockReview();
        User user = Helpers.createMockUserJunkie();

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis()));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 1000000000));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.create(review, user));
    }

    @Test
    public void create_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Review review = Helpers.createMockReview();
        review.getPhoto().getContest().getJury().add(review.getCreatedBy());

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis()));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));

        //Act, Assert
        reviewService.create(review, review.getCreatedBy());

        Mockito.verify(reviewRepository, Mockito.times(1))
                .create(Mockito.any(Review.class), Mockito.any(Photo.class));
    }

    @Test
    public void update_Should_Throw_When_PhaseIsNotSecond() {
        //Arrange
        Review review = Helpers.createMockReview();

        //Act, Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> reviewService.update(review, review.getCreatedBy()));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotJury() {
        //Arrange
        Review review = Helpers.createMockReview();
        User user = Helpers.createMockUserJunkie();

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis()));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.update(review, user));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreator() {
        //Arrange
        Review review = Helpers.createMockReview();
        User user = Helpers.createMockUserOrganizer();
        user.setId(4);
        review.getPhoto().getContest().getJury().add(user);

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis()));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.update(review, user));
    }

    @Test
    public void update_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Review review = Helpers.createMockReview();
        review.getPhoto().getContest().getJury().add(review.getCreatedBy());
        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis() + 1));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));

        reviewService.update(review, review.getCreatedBy());

        //Act, Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .update(Mockito.any(Review.class), Mockito.any(Photo.class));
    }


    @Test
    public void delete_Should_Throw_When_PhaseIsNotSecond() {
        //Arrange
        Review review = Helpers.createMockReview();

        //Act, Assert
        Assertions.assertThrows(UnsupportedOperationException.class,
                () -> reviewService.delete(review, review.getCreatedBy()));
    }

    @Test
    public void delete_Should_Throw_When_UserIsNotJury() {
        //Arrange
        Review review = Helpers.createMockReview();

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis() + 1));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.delete(review, review.getCreatedBy()));
    }

    @Test
    public void delete_Should_Throw_When_UserIsNotCreator() {
        //Arrange
        Review review = Helpers.createMockReview();
        User user = Helpers.createMockUserOrganizer();
        user.setId(4);
        review.getPhoto().getContest().getJury().add(user);

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis() + 1));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));


        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> reviewService.delete(review, user));
    }

    @Test
    public void delete_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Review review = Helpers.createMockReview();

        Contest contest = review.getPhoto().getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis() + 1));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 123456123));


        reviewService.delete(review, review.getCreatedBy());

        //Act, Assert
        Mockito.verify(reviewRepository, Mockito.times(1))
                .delete(Mockito.any(Review.class), Mockito.any(Photo.class));
    }


}
