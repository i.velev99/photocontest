package com.finalproject.demo.services;

import com.finalproject.demo.Helpers;
import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import com.finalproject.demo.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getAll_Should_CallRepository() {
        userService.getAll();
        Mockito.verify(userRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {

        //Arrange
        Mockito.when(userRepository.getById(1))
                .thenReturn(Helpers.createMockUserJunkie());

        //Act
        User result = userService.getById(1);

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("ivan", result.getUserCredentials().getUsername());
        Assertions.assertEquals("password1", result.getUserCredentials().getPassword());
        Assertions.assertTrue(result.getUserCredentials().isPhotoJunkie());
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_MatchExists() {

        //Arrange
        Mockito.when(userRepository.getByUsername("ivan"))
                .thenReturn(Helpers.createMockUserJunkie());

        //Act
        User result = userService.getByUsername("ivan");

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Velev", result.getLastName());
        Assertions.assertEquals("ivan", result.getUserCredentials().getUsername());
        Assertions.assertEquals("password1", result.getUserCredentials().getPassword());
        Assertions.assertTrue(result.getUserCredentials().isPhotoJunkie());
    }

    @Test
    public void create_Should_Throw_When_UserWithSameUsernameExists() {
        //Arrange
        User user = Helpers.createMockUserJunkie();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenReturn(user);

        User secondUser = Helpers.createMockUserJunkie();
        secondUser.setId(2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(secondUser));
    }

    @Test
    public void create_Should_CallRepository_when_UserCredentialsUnique() {

        var user = Helpers.createMockUserJunkie();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenThrow(EntityNotFoundException.class);

        userService.create(user);

        Mockito.verify(userRepository, Mockito.times(1))
                .create(user);
    }

    @Test
    public void update_Should_Throw_When_UserWithSameUsernameExists() {
        //Arrange
        User user = Helpers.createMockUserJunkie();

        User secondUser = Helpers.createMockUserJunkie();

        User thirdUser = Helpers.createMockUserJunkie();
        thirdUser.setId(3);

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenReturn(thirdUser);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(secondUser, user));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotCreator() {
        //Arrange
        User user = Helpers.createMockUserJunkie();
        User testUser = Helpers.createMockUserOrganizer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.update(user, testUser));
    }

    @Test
    public void update_Should_CallRepository_when_UserCredentialsUnique() {

        var user = Helpers.createMockUserJunkie();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenThrow(EntityNotFoundException.class);


        userService.update(user,user);

        Mockito.verify(userRepository, Mockito.times(1))
                .update(user);
    }

    @Test
    public void delete_Should_Throw_When_UserIsNotCreator() {
        //Arrange
        User user = Helpers.createMockUserJunkie();
        User testUser = Helpers.createMockUserOrganizer();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> userService.delete(user, testUser));
    }

    @Test
    public void delete_Should_CallRepository_when_UserCredentialsUnique() {

        var user = Helpers.createMockUserJunkie();

        userService.delete(user,user);

        Mockito.verify(userRepository, Mockito.times(1))
                .delete(user);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_FirstNameParameterOnly() {
        var user = Helpers.createMockUserJunkie();

        UserFilterParameters ufp = new UserFilterParameters(Optional.of(user.getFirstName()),Optional.empty(),Optional.empty());

        userService.filter(ufp);

        Mockito.verify(userRepository, Mockito.times(1))
                .filter(ufp);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_LastNameParameterOnly() {
        var user = Helpers.createMockUserJunkie();

        UserFilterParameters ufp = new UserFilterParameters(Optional.empty(),Optional.of(user.getLastName()),Optional.empty());

        userService.filter(ufp);

        Mockito.verify(userRepository, Mockito.times(1))
                .filter(ufp);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_UsernameParameterOnly() {
        var user = Helpers.createMockUserJunkie();

        UserFilterParameters ufp = new UserFilterParameters(Optional.empty(),Optional.empty(),Optional.of(user.getUserCredentials().getUsername()));

        userService.filter(ufp);

        Mockito.verify(userRepository, Mockito.times(1))
                .filter(ufp);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_NoParameters() {
        var user = Helpers.createMockUserJunkie();

        UserFilterParameters ufp = new UserFilterParameters(Optional.empty(),Optional.empty(),Optional.of(user.getUserCredentials().getUsername()));

        userService.filter(ufp);

        Mockito.verify(userRepository, Mockito.times(1))
                .filter(ufp);
    }

    @Test
    public void pointsUntilNextRank_Should_CallRepository_When_ValidParameters() {
        var user = Helpers.createMockUserJunkie();

        userService.pointsUntilNextRank(user);

        Mockito.verify(userRepository, Mockito.times(1))
                .pointsUntilNextRank(user);
    }

}
