package com.finalproject.demo.services;

import com.finalproject.demo.Helpers;
import com.finalproject.demo.models.Category;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Type;
import com.finalproject.demo.repositories.ContestRepository;
import com.finalproject.demo.repositories.PhotoRepository;
import com.finalproject.demo.repositories.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTests {

    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private PhotoRepository photoRepository;

    @Mock
    private ContestRepository contestRepository;

    @InjectMocks
    private ContestServiceImpl contestService;

    @Test
    public void getAll_Should_CallRepository() {
        contestService.getAll();
        Mockito.verify(contestRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnContest_When_MatchExists() {

        //Arrange
        Mockito.when(contestRepository.getById(1))
                .thenReturn(Helpers.createMockContest());

        //Act
        Contest result = contestService.getById(1);
        Category category = Helpers.createMockCategory();
        Type type = Helpers.createMockType();

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("The Awesomeness of Nature", result.getTitle());
        Assertions.assertEquals(category, result.getCategory());
        Assertions.assertEquals(type, result.getType());
        Assertions.assertEquals("cover photo", result.getCoverPhoto());
    }
}
