package com.finalproject.demo.services;

import com.finalproject.demo.Helpers;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.repositories.PhotoRepository;
import com.finalproject.demo.repositories.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PhotoServiceTests {

    @Mock
    private ReviewRepository reviewRepository;

    @Mock
    private PhotoRepository photoRepository;

    @InjectMocks
    private PhotoServiceImpl photoService;

    @Test
    public void getAll_Should_CallRepository() {
        photoService.getAll();
        Mockito.verify(photoRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnReview_When_MatchExists() {

        //Arrange
        Mockito.when(photoRepository.getById(1))
                .thenReturn(Helpers.createMockPhoto());

        //Act
        Photo result = photoService.getById(1);
        User user = Helpers.createMockUserJunkie();
        Contest contest = Helpers.createMockContest();

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Pirin", result.getTitle());
        Assertions.assertEquals("amazing place", result.getStory());
        Assertions.assertEquals("photo url", result.getPhotoUrl());
        Assertions.assertEquals(user,result.getOwner());
        Assertions.assertEquals(contest,result.getContest());
    }

    @Test
    public void create_Should_Throw_When_UserIsNotPhotoJunkie() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserOrganizer();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.create(photo,user));
    }

    @Test
    public void create_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserJunkie();
        Contest contest = photo.getContest();
        contest.setStartDate(new Date(System.currentTimeMillis()));
        contest.setFirstPhaseFinalDate(new Date(System.currentTimeMillis() + 1000000));
        contest.setSecondPhaseFinalDate(new Date(System.currentTimeMillis() + 1000000000));
        photoService.create(photo, user);

        //Act, Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .create(Mockito.any(Photo.class));
    }

    @Test
    public void update_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserJunkie();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.update(photo,user));
    }

    @Test
    public void update_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserOrganizer();

        photoService.update(photo,user);

        //Act, Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .update(Mockito.any(Photo.class));
    }

    @Test
    public void delete_Should_Throw_When_UserIsNotOrganizer() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserJunkie();

        //Act, Assert
        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> photoService.delete(photo,user));
    }

    @Test
    public void delete_Should_CallRepository_When_ValidParameters() {
        //Arrange
        Photo photo = Helpers.createMockPhoto();
        User user = Helpers.createMockUserOrganizer();

        photoService.delete(photo,user);

        //Act, Assert
        Mockito.verify(photoRepository, Mockito.times(1))
                .delete(Mockito.any(Photo.class));
    }

    @Test
    public void filter_Should_CallRepository_When_NoParameters() {
        var photo = Helpers.createMockPhoto();

        PhotoFilterParameters pfp = new PhotoFilterParameters(Optional.empty(),Optional.empty());

        photoService.filter(pfp);

        Mockito.verify(photoRepository, Mockito.times(1))
                .filter(pfp);
    }
}
