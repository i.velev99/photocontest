package com.finalproject.demo;

import com.finalproject.demo.models.*;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

public class Helpers {

    public static UserCredentials createMockUserCredentialsJunkie() {
        return new UserCredentials("ivan", "password1");
    }

    public static UserCredentials createMockUserCredentialsOrganizer() {
        return new UserCredentials("mitaka", "password2");
    }

    public static User createMockUserJunkie() {
        User user = new User(1, "Ivan", "Velev", createMockUserCredentialsJunkie());
        user.getUserCredentials().setRoles(Set.of(createMockRoleJunkie()));
        return user;
    }

    public static User createMockUserOrganizer() {
        User user = new User(2, "Dimitar", "Iliev", createMockUserCredentialsOrganizer());
        user.getUserCredentials().setRoles(Set.of(createMockRoleOrganizer()));
        return user;

    }
    public static Category createMockCategory() {
        return new Category(1,"Nature");
    }

    public static Role createMockRoleJunkie() {
        Role role = new Role();
        role.setId(1);
        role.setName("Photo Junkie");
        return role;
    }
    public static Role createMockRoleOrganizer() {
        Role role = new Role();
        role.setId(2);
        role.setName("Organizer");
        return role;
    }

    public static Type createMockType() {
        Type type = new Type();
        type.setId(1);
        type.setName("Open");
        return type;
    }

    public static Contest createMockContest() {
        return new Contest(1, "The Awesomeness of Nature",createMockCategory(), createMockType(),
                new Date(2021, Calendar.JANUARY, 3),
                new Date(2021, Calendar.JANUARY, 5),
                new Date(2021, Calendar.JANUARY, 5), "cover photo");
    }

    public static Photo createMockPhoto() {

        Photo photo = new Photo(1, "Pirin", "amazing place",
                createMockContest(),
                createMockUserJunkie(),
                new Date(System.currentTimeMillis()),
                "photo url");

        return photo;
    }

    public static Review createMockReview() {
        return new Review(1, 5, "Nice photo", createMockUserOrganizer(),createMockPhoto());
    }
}
