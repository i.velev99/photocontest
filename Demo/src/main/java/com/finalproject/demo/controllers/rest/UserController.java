package com.finalproject.demo.controllers.rest;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.UserDto;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import com.finalproject.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserController(UserService userService,
                          ModelMapper modelMapper,
                          AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<User> filter(@RequestParam(required = false) Optional<String> firstName,
                             @RequestParam(required = false) Optional<String> lastName,
                             @RequestParam(required = false) Optional<String> username) {
        UserFilterParameters filterParameters = modelMapper.fromParametersList(firstName, lastName, username);
        return userService.filter(filterParameters);

    }

    @GetMapping("/{id}")
    public User getById(@PathVariable int id) {
        return userService.getById(id);
    }

    @GetMapping("/name/{name}")
    public User getByUsername(@PathVariable String name) {
        return userService.getByUsername(name);
    }

    @PostMapping
    public User create(@Valid @RequestBody UserDto userDto) {
        User user = modelMapper.fromDto(userDto);
        userService.create(user);
        return user;

    }

    @GetMapping("/next-rank")
    public int pointsUntilNextRank(@RequestHeader HttpHeaders headers){
        User user = authenticationHelper.tryGetUser(headers);
        return userService.pointsUntilNextRank(user);
    }

    @PutMapping("/{id}")
    public User update(@RequestHeader HttpHeaders headers,
                       @PathVariable int id,
                       @Valid @RequestBody UserDto userDto) {

        User creator = authenticationHelper.tryGetUser(headers);
        User user = modelMapper.fromDto(userDto, id);
        creator.getUserCredentials().setUsername(user.getUserCredentials().getUsername());
        userService.update(user, creator);
        return user;

    }

    @DeleteMapping("/{id}")
    public User delete(@RequestHeader HttpHeaders headers,
                       @PathVariable int id) {
        User creator = authenticationHelper.tryGetUser(headers);
        User user = getById(id);
        userService.delete(user, creator);
        return user;
    }
}


