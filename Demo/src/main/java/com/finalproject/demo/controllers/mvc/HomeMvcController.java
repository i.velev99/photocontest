package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;

    @Autowired
    public HomeMvcController(AuthenticationHelper authenticationHelper, PhotoService photoService) {
        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
    }

    @ModelAttribute("winingPhotos")
    public List<Photo> getAllCategories() {
        return photoService.getAll().stream().sorted(Comparator.comparingDouble(Photo::getScore).reversed()).collect(Collectors.toList());
    }

    @GetMapping
    public String showHomePage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthenticationFailureException | UnauthorizedOperationException ignored) {

        }
        return "index";
    }

    @GetMapping("/ranks/about")
    public String showRanksAboutPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthenticationFailureException | UnauthorizedOperationException ignored) {

        }
        return "ranks-about";
    }
}
