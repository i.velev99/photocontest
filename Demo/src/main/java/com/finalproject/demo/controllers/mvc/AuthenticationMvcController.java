package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.LoginDto;
import com.finalproject.demo.models.dto.RegisterDto;
import com.finalproject.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.finalproject.demo.constants.Constants.PASSWORD_CONFIRM_ERROR_MESSAGE;

@Controller
@RequestMapping
public class AuthenticationMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final ModelMapper modelMapper;
    private final UserService userService;

    @Autowired
    public AuthenticationMvcController(AuthenticationHelper authenticationHelper,
                                       ModelMapper modelMapper,
                                       UserService userService) {
        this.authenticationHelper = authenticationHelper;
        this.modelMapper = modelMapper;
        this.userService = userService;
    }

    @GetMapping("/login")
    public String showLoginPage(Model model, HttpSession session) {
        model.addAttribute("login", new LoginDto());

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch ( AuthenticationFailureException | UnauthorizedOperationException ignored) {
        }

        return "login";
    }

    @PostMapping("/login")
    public String handleLogin(@Valid @ModelAttribute("login") LoginDto login,
                              BindingResult bindingResult,
                              HttpSession session) {

        if (bindingResult.hasErrors()) {
            return "login";
        }

        try {
            authenticationHelper.verifyAuthentication(login.getUsername(), login.getPassword());
            session.setAttribute("currentUser", login.getUsername());
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            bindingResult.rejectValue("username", "auth_error", e.getMessage());
            return "login";
        }
    }

    @GetMapping("/logout")
    public String handleLogout(HttpSession session) {
        session.removeAttribute("currentUser");
        return "redirect:/";
    }


    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("register", new RegisterDto());
        return "register";
    }

    @PostMapping("/register")
    public String handleRegister(@Valid @ModelAttribute("register") RegisterDto register,
                                 BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!register.getPassword().equals(register.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", PASSWORD_CONFIRM_ERROR_MESSAGE);
            return  "register";
        }

        try {
            User user = modelMapper.fromDto(register);
            userService.create(user);
            return "redirect:/login";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }
    }
}
