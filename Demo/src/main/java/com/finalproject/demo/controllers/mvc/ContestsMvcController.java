package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.ContestDto;
import com.finalproject.demo.models.dto.PhotoDto;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.services.ContestService;
import com.finalproject.demo.services.PhotoService;
import com.finalproject.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.*;

@Controller
@RequestMapping("/contests")
public class ContestsMvcController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;
    private final UserService userService;
    private final ModelMapper modelMapper;


    @Autowired
    public ContestsMvcController(ContestService contestService,
                                 AuthenticationHelper authenticationHelper,
                                 PhotoService service,
                                 UserService userService, ModelMapper modelMapper) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.photoService = service;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public String showSingleContest(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            Contest contest = contestService.getById(id);
            PhotoFilterParameters pfp = new PhotoFilterParameters(Optional.of(contest.getId()), Optional.empty());
            List<Photo> photos = photoService.filter(pfp);
            model.addAttribute("photos", photos);
            model.addAttribute("contest", contest);

            PhotoFilterParameters userFilterPhotos = new PhotoFilterParameters(Optional.of(contest.getId()), Optional.of(currentUser.getId()));
            List<Photo> userPhotos = photoService.filter(userFilterPhotos);
            model.addAttribute("userPhotos", userPhotos);
            return "contest";

        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/enroll")
    public String enroll(@PathVariable int id, Model model, HttpSession session) {
        try {
            Contest contest = contestService.getById(id);
            model.addAttribute("contest", contest);
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            contestService.join(contest, currentUser, false);
            return "redirect:/contests/{id}";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnsupportedOperationException |
                UnauthorizedOperationException |
                EntityNotFoundException |
                DuplicateFormatFlagsException e) {

            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }


    @GetMapping
    public String filter(@RequestParam(value = "filter", required = false) Optional<String> filter,
                         HttpSession session, Model model) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            List<Contest> contests = new ArrayList<>();
            if (currentUser.getUserCredentials().isOrganizer()) {
                contests = contestService.getAll();
                if (filter.isPresent()) {
                    if (filter.get().equals(ALL)) {
                        contests = contestService.getAll();
                    }
                    if (filter.get().equals(TYPE_OPEN)) {
                        ContestFilterParameters cfp = new ContestFilterParameters(Optional.empty(), Optional.of(1));
                        contests = contestService.filter(cfp);
                    }
                    if (filter.get().equals(TYPE_INVITATIONAL)) {
                        ContestFilterParameters cfp = new ContestFilterParameters(Optional.empty(), Optional.of(2));
                        contests = contestService.filter(cfp);
                    }
                    if (filter.get().equals(FIRST_PHASE_QUERY)) {
                        contests = contestService.filterByPhase(contests, PHASE_ONE);
                    }
                    if (filter.get().equals(SECOND_PHASE_QUERY)) {
                        contests = contestService.filterByPhase(contests, PHASE_TWO);
                    }
                    if (filter.get().equalsIgnoreCase(FINISHED_PHASE)) {
                        contests = contestService.filterByPhase(contests, FINISHED_PHASE);
                    }
                }
            }

            if (currentUser.getUserCredentials().isPhotoJunkie()) {
                contests = contestService.getAll();
                if (filter.isPresent()) {
                    if (filter.get().equals(TYPE_OPEN)) {
                        ContestFilterParameters cfp = new ContestFilterParameters(Optional.empty(), Optional.of(1));
                        contests = contestService.filter(cfp);
                    }
                    if (filter.get().equals(USER_PARTICIPATE)) {
                        contests = contests.stream().filter(contest -> contest.getParticipants().contains(currentUser)
                                && contest.getSecondPhaseFinalDate().after(Date.from(Instant.now()))).collect(Collectors.toList());
                    }
                    if (filter.get().equalsIgnoreCase(FINISHED_PHASE)) {
                        contests = contests.stream().filter(contest -> contest.getParticipants().contains(currentUser)).collect(Collectors.toList());
                        contests = contestService.filterByPhase(contests, FINISHED_PHASE);
                    }
                    if (filter.get().equals(JURY)) {
                        contests = contests.stream().filter(contest -> contest.getJury().contains(currentUser)).collect(Collectors.toList());
                    }
                }
            }
            Collections.sort(contests);
            model.addAttribute("contests", contests);
            model.addAttribute("currentUser", currentUser);
            return "contests";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/upload")
    public String showUploadImage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(id);
            model.addAttribute("contest", contest);
            model.addAttribute("photoDto", new PhotoDto());
            model.addAttribute("currentUser", currentUser);

            return "upload-photo-to-contest";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("{id}/upload")
    public String uploadImage(@PathVariable int id,
                              @RequestParam("imageFile") MultipartFile imageFile,
                              @ModelAttribute("photoDto") PhotoDto dto,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {

        if (bindingResult.hasErrors()) {
            return "upload-photo-to-contest";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            Photo photo = modelMapper.fromDto(dto, imageFile, currentUser, id);
            photoService.create(photo, currentUser);
            photoService.saveImage(imageFile);
            return "redirect:/contests/{id}";
        } catch (IOException | DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        }
    }

    @GetMapping("/{id}/invite")
    public String showInvitationPage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            Contest contest = contestService.getById(id);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("contestDto", new ContestDto());
            model.addAttribute("contest", contest);

            List<User> users = userService.getAll();
            List<User> junkies = new ArrayList<>();
            for (User junkie : users) {
                if (!contest.getParticipants().contains(junkie) && !contest.getJury().contains(junkie)) {
                    junkies.add(junkie);
                }
            }
            model.addAttribute("junkies", junkies);
            return "add-junkies";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/invite")
    public String handleInvitationPage(@PathVariable int id, @ModelAttribute("contestDto") ContestDto dto,
                                       BindingResult bindingResult,
                                       Model model,
                                       HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "/{id}/invite";
        }

        try {
            User user = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            Contest contest = contestService.getById(id);
            Set<User> set = new HashSet<>();
            for (Integer i : dto.getParticipants()) {
                User participant = userService.getById(i);
                set.add(participant);
            }
            set.forEach(u -> contestService.join(contest, u, true));
            return "redirect:/contests/{id}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }
    @GetMapping("/about")
    public String showContestAboutPage(HttpSession session, Model model){
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
        } catch (AuthenticationFailureException | UnauthorizedOperationException ignored) {

        }
        return "contest-about";
    }
}

