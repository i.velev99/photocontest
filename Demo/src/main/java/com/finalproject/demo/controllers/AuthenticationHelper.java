package com.finalproject.demo.controllers;

import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

import java.util.Set;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.*;

@Component
public class AuthenticationHelper {

    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        checkAuthorization(headers);
        try {
            String username = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return userService.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, INVALID_USERNAME);
        }
    }

    public User tryGetUser(HttpSession session) {
        String currentUserUsername = (String) session.getAttribute("currentUser");

        if (currentUserUsername == null) {
            throw new AuthenticationFailureException(NO_LOGGED_IN_USER);
        }

        try {
            return userService.getByUsername(currentUserUsername);
        } catch (EntityNotFoundException e) {
            throw new UnauthorizedOperationException(NO_LOGGED_IN_USER);
        }
    }

    private void checkAuthorization(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {

            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    AUTHORIZATION_ERROR_MESSAGE);
        }
    }

    public User verifyAuthorization(HttpSession session, String role) {
        User user = tryGetUser(session);

        Set<String> userRoles = user.getUserCredentials()
                .getRoles()
                .stream()
                .map(r -> r.getName().toLowerCase())
                .collect(Collectors.toSet());

        if (!userRoles.contains(role.toLowerCase())) {
            throw new UnauthorizedOperationException(USER_AUTHORIZATION_ERROR_MESSAGE);
        }

        return user;
    }

    public User verifyAuthentication(String username, String password) {
        try {
            User user = userService.getByUsername(username);
            if (!user.getUserCredentials().getPassword().equals(password)) {
                throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthenticationFailureException(AUTHENTICATION_FAILURE_MESSAGE);
        }
    }


}
