package com.finalproject.demo.controllers.rest;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.ContestDto;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;
import com.finalproject.demo.services.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ContestController(ContestService contestService,
                             ModelMapper modelMapper,
                             AuthenticationHelper authenticationHelper) {
        this.contestService = contestService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<Contest> filter(@RequestParam(required = false) Optional<Integer> categoryId,
                                @RequestParam(required = false) Optional<Integer> typeId) {

        ContestFilterParameters filterParameters = modelMapper.fromParametersList(categoryId, typeId);
        return contestService.filter(filterParameters);
    }

    @GetMapping("/{id}")
    public Contest getById(@PathVariable int id) {
        return contestService.getById(id);
    }

    @PostMapping
    public Contest create(@RequestHeader HttpHeaders headers,
                          @Valid @RequestBody ContestDto contestDto) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Contest contest = modelMapper.fromDto(contestDto);
        contestService.create(contest, organizer);
        return contest;
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers,
                          @PathVariable int id,
                          @Valid @RequestBody ContestDto contestDto) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Contest contest = modelMapper.fromDto(contestDto, id);
        contestService.update(contest, organizer);
        return contest;
    }

    @DeleteMapping("/{id}")
    public Contest delete(@RequestHeader HttpHeaders headers,
                          @PathVariable int id) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Contest contest = getById(id);
        contestService.delete(contest, organizer);
        return contest;
    }
}
