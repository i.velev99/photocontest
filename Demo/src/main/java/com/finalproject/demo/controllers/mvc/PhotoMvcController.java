package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.repositories.ReviewRepository;
import com.finalproject.demo.services.ContestService;
import com.finalproject.demo.services.PhotoService;
import com.finalproject.demo.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RequestMapping
@Controller
public class PhotoMvcController {


    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;
    private final ContestService contestService;
    private final ReviewService reviewService;

    @Autowired
    public PhotoMvcController(AuthenticationHelper authenticationHelper,
                              PhotoService photoService,
                              ContestService contestService, ReviewService reviewService) {
        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
        this.contestService = contestService;
        this.reviewService = reviewService;
    }

    @GetMapping("/contests/{contestId}/photos/{photoId}")
    public String showPhotoPage(@PathVariable int contestId,
                                @PathVariable int photoId,
                                Model model,
                                HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(contestId);
            Photo photo = photoService.getById(photoId);
            Set<Review> photoReviews= reviewService.getAll()
                    .stream()
                    .filter(review -> review.getPhoto().getId() == photo.getId())
                    .collect(Collectors.toSet());
            List<Review> reviewsFilteredByUserAndPhoto = reviewed(photo,currentUser);
            model.addAttribute("reviewed", reviewsFilteredByUserAndPhoto);
            model.addAttribute("reviews", photoReviews);
            model.addAttribute("contest", contest);
            model.addAttribute("photo", photo);
            model.addAttribute("currentUser", currentUser);
            return "photo";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        }catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    public List<Review> reviewed(Photo photo, User user) {
        return reviewService.getAll()
                .stream()
                .filter(review -> review.getPhoto().getId() == photo.getId())
                .filter(review -> review.getCreatedBy().getId() == user.getId())
                .sorted(Comparator.comparingDouble(Review::getPoints).reversed())
                .collect(Collectors.toList());
    }

}
