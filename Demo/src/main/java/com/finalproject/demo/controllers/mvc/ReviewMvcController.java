package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.CreateReviewDto;
import com.finalproject.demo.models.dto.PhotoDto;
import com.finalproject.demo.models.dto.ReviewDto;
import com.finalproject.demo.services.ContestService;
import com.finalproject.demo.services.PhotoService;
import com.finalproject.demo.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.USER_REVIEWED_PHOTO_ERROR_MESSAGE;

@RequestMapping
@Controller
public class ReviewMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final PhotoService photoService;
    private final ContestService contestService;
    private final ModelMapper modelMapper;
    private final ReviewService reviewService;

    @Autowired
    public ReviewMvcController(AuthenticationHelper authenticationHelper,
                               PhotoService photoService,
                               ContestService contestService,
                               ModelMapper modelMapper,
                               ReviewService reviewService) {

        this.authenticationHelper = authenticationHelper;
        this.photoService = photoService;
        this.contestService = contestService;
        this.modelMapper = modelMapper;
        this.reviewService = reviewService;
    }


    @GetMapping("/contests/{contestId}/photos/{photoId}/review")
    public String showCreateReview(@PathVariable int contestId,
                                  @PathVariable int photoId,
                                  Model model,
                                  HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(contestId);
            Photo photo = photoService.getById(photoId);
            model.addAttribute("contest", contest);
            model.addAttribute("photo", photo);
            model.addAttribute("reviewDto", new CreateReviewDto());
            model.addAttribute("currentUser", currentUser);

            return "review";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/contests/{contestId}/photos/{photoId}/review")
    public String handleCreateReview(@PathVariable int contestId,
                              @PathVariable int photoId,
                              @ModelAttribute("reviewDto") CreateReviewDto dto,
                              BindingResult bindingResult,
                              HttpSession session,
                              Model model) {

        if (bindingResult.hasErrors()) {
            return "review";
        }

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            Contest contest = contestService.getById(contestId);
            Photo photo = photoService.getById(photoId);
            Set<Review> photoReviews= reviewService.getAll()
                    .stream()
                    .filter(review -> review.getPhoto().equals(photo))
                    .collect(Collectors.toSet());
            List<Review> reviewsFilteredByUserAndPhoto = reviewed(photo,currentUser);
            model.addAttribute("reviewed", reviewsFilteredByUserAndPhoto);
            model.addAttribute("reviews", photoReviews);
            model.addAttribute("contest", contest);
            model.addAttribute("photo", photo);
            model.addAttribute("currentUser", currentUser);
            Review review = modelMapper.fromDto(dto, currentUser);
            reviewService.create(review, currentUser);
            return "redirect:/contests/{contestId}/photos/{photoId}";
        } catch (DuplicateEntityException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        }
    }

    public List<Review> reviewed(Photo photo, User user) {
       return reviewService.getAll()
                .stream()
                .filter(review -> review.getPhoto().getId() == photo.getId())
                .filter(review -> review.getCreatedBy().getId() == user.getId())
                .sorted(Comparator.comparingDouble(Review::getPoints).reversed())
                .collect(Collectors.toList());
    }
}
