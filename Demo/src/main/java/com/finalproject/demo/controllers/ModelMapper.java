package com.finalproject.demo.controllers;

import com.finalproject.demo.constants.Constants;
import com.finalproject.demo.models.*;
import com.finalproject.demo.models.dto.*;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import com.finalproject.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

@Component
public class ModelMapper {

    public static final String WRONG_CATEGORY_MESSAGE = "The category is wrong!";
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ContestRepository contestRepository;
    private final TypeRepository typeRepository;
    private final CategoryRepository categoryRepository;
    private final PhotoRepository photoRepository;
    private final ReviewRepository reviewRepository;

    @Autowired
    public ModelMapper(UserRepository userRepository,
                       RoleRepository roleRepository,
                       ContestRepository contestRepository,
                       TypeRepository typeRepository,
                       CategoryRepository categoryRepository,
                       PhotoRepository photoRepository,
                       ReviewRepository reviewRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.contestRepository = contestRepository;
        this.typeRepository = typeRepository;
        this.categoryRepository = categoryRepository;
        this.photoRepository = photoRepository;
        this.reviewRepository = reviewRepository;
    }

    public UserFilterParameters fromParametersList(Optional<String> firstName,
                                                   Optional<String> lastName,
                                                   Optional<String> username) {
        return new UserFilterParameters(firstName,lastName,username);
    }

    public ContestFilterParameters fromParametersList(Optional<Integer> categoryId,
                                                      Optional<Integer> typeId) {
        return new ContestFilterParameters(categoryId,typeId);
    }

    public PhotoFilterParameters fromParametersListPhoto(Optional<Integer> contestId,
                                                    Optional<Integer> ownerId) {
        return new PhotoFilterParameters(contestId,ownerId);
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(user, userDto);
        return user;
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(user, userDto);
        return user;
    }

    private void dtoToObject(User user, UserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUserCredentials(fromDto(userDto.getUserCredentialsDto()));
        user.setPoints(0);
        user.getUserCredentials().setRoles(Set.of(roleRepository.getByName(Constants.PHOTO_JUNKIE_ROLE)));
    }

    public UserCredentials fromDto(UserCredentialsDto userCredentialsDto) {
        UserCredentials userCredentials = new UserCredentials();
        dtoToObject(userCredentials,userCredentialsDto);
        return userCredentials;
    }

    private void dtoToObject(UserCredentials userCredentials, UserCredentialsDto userCredentialsDto) {
        userCredentials.setUsername(userCredentialsDto.getUsername());
        userCredentials.setPassword(userCredentialsDto.getPassword());
    }

    public Contest fromDto(ContestDto contestDto) {
        Contest contest = new Contest();
        dtoToObject(contest, contestDto);
        return contest;
    }

    public Contest fromDto(ContestDto contestDto, int id) {
        Contest contest = contestRepository.getById(id);
        dtoToObject(contest, contestDto);
        return contest;
    }

    private void dtoToObject(Contest contest, ContestDto contestDto) {
        contest.setTitle(contestDto.getTitle());
        contest.setCategory(categoryRepository.getById(contestDto.getCategoryId()));
        contest.setType(typeRepository.getById(contestDto.getTypeId()));
        contest.setStartDate(contestDto.getStartDate());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(contest.getStartDate());
        calendar.add(Calendar.MONTH, contestDto.getFirstPhaseTimeLimit());

        contest.setFirstPhaseFinalDate(calendar.getTime());

        calendar.setTime(contest.getFirstPhaseFinalDate());
        calendar.add(Calendar.HOUR_OF_DAY, contestDto.getSecondPhaseTimeLimit());

        contest.setSecondPhaseFinalDate(calendar.getTime());

        Set<User> jury = userRepository.getAll().stream()
                .filter(user -> user.getUserCredentials().getRoles().stream()
                        .map(Role::getName).collect(Collectors.toSet()).contains("Organizer")).collect(Collectors.toSet());

        for (String username : contestDto.getJury()) {
            User junkies = userRepository.getByUsername(username);
            jury.add(junkies);
        }

        contest.setJury(jury);
        contest.setScored(false);
        contest.setCoverPhoto(contestDto.getPhotoUrl());
    }

    public Photo fromDto(PhotoDto photoDto) {
        Photo photo = new Photo();
        dtoToObject(photo, photoDto);
        return photo;
    }

    public Photo fromDto(PhotoDto photoDto , MultipartFile imageFile , User user , int contestId) {
        Photo photo = new Photo();
        dtoToObject(photo, photoDto , imageFile, user, contestId);
        return photo;
    }

    public Photo fromDto(PhotoDto photoDto, int id) {
        Photo photo = photoRepository.getById(id);
        dtoToObject(photo, photoDto);
        return photo;
    }

    private void dtoToObject(Photo photo, PhotoDto photoDto) {
        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
        photo.setPhotoUrl(photoDto.getPhotoUrl());
        photo.setContest(contestRepository.getById(photoDto.getContestId()));
        photo.setUploadDate(Date.from(Instant.now()));
        photo.setPlace(0);
        photo.setScore(0);

    }private void dtoToObject(Photo photo, PhotoDto photoDto, MultipartFile imageFile ,User user , int contestId) {
        photo.setTitle(photoDto.getTitle());
        photo.setStory(photoDto.getStory());
        photo.setPhotoUrl("~/assets/img/" + imageFile.getOriginalFilename());
        photo.setUploadDate(Date.from(Instant.now()));
        photo.setContest(contestRepository.getById(contestId));
        photo.setOwner(user);
        photo.setPlace(0);
        photo.setScore(0);
    }


    public Review fromDto(ReviewDto reviewDto) {
        Review review = new Review();
        dtoToObject(review, reviewDto);
        return review;
    }
    public Review fromDto(CreateReviewDto reviewDto, User owner) {
        Review review = new Review();
        dtoToObject(review, reviewDto, owner);
        return review;
    }

    public Review fromDto(ReviewDto reviewDto, int id) {
        Review review = reviewRepository.getById(id);
        dtoToObject(review, reviewDto);
        return review;
    }

    private void dtoToObject(Review review, ReviewDto reviewDto) {
        review.setPoints(reviewDto.getPoints());
        review.setReview(reviewDto.getReview());
        review.setPhoto(photoRepository.getById(reviewDto.getPhotoId()));
    }
    private void dtoToObject(Review review, CreateReviewDto reviewDto, User owner) {
        if (reviewDto.isInappropriate()){
            review.setPoints(0);
            review.setReview(WRONG_CATEGORY_MESSAGE);
            review.setCreatedBy(owner);
            review.setPhoto(photoRepository.getById(reviewDto.getPhotoId()));
            return;
        }
        review.setPoints(reviewDto.getPoints());
        review.setReview(reviewDto.getReview());
        review.setCreatedBy(owner);
        review.setPhoto(photoRepository.getById(reviewDto.getPhotoId()));
    }

    public User fromDto(RegisterDto registerDto) {
        User user = new User();
        UserCredentials uc = new UserCredentials();
        uc.setUsername(registerDto.getUsername());
        uc.setPassword(registerDto.getPassword());
        user.setUserCredentials(uc);
        user.setFirstName(registerDto.getFirstName());
        user.setLastName(registerDto.getLastName());
        user.getUserCredentials().setRoles(Set.of(roleRepository.getByName(Constants.PHOTO_JUNKIE_ROLE)));
        user.setPoints(0);
        return user;
    }

    public EditUserDto fromUser(User user) {
        EditUserDto userDto = new EditUserDto();
        dtoToObject(user,userDto);
        return userDto;
    }

    public void dtoToObject(User user, EditUserDto dto) {
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setPassword(user.getUserCredentials().getPassword());
        dto.setRoles(user.getUserCredentials().getRoles());
    }

    public User fromDto(EditUserDto dto, int id) {
        User user = userRepository.getById(id);
        user.setFirstName(dto.getFirstName());
        user.setLastName(dto.getLastName());
        user.getUserCredentials().setPassword(dto.getPassword());
        return user;
    }

    public Contest fromDto(CreateContestDto dto) {
        Contest contest = new Contest();
        dtoToObject(contest,dto);
        return contest;
    }

    private void dtoToObject(Contest contest, CreateContestDto dto) {
        contest.setTitle(dto.getTitle());
        contest.setCategory(categoryRepository.getById(dto.getCategoryId()));
        contest.setType(typeRepository.getById(dto.getTypeId()));
        contest.setStartDate(dto.getStartDate());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(contest.getStartDate());
        calendar.add(Calendar.DAY_OF_WEEK, dto.getFirstPhaseTimeLimit());

        contest.setFirstPhaseFinalDate(calendar.getTime());

        calendar.setTime(contest.getFirstPhaseFinalDate());
        calendar.add(Calendar.HOUR_OF_DAY, dto.getSecondPhaseTimeLimit());

        contest.setSecondPhaseFinalDate(calendar.getTime());

        Set<User> jury = userRepository.getAll().stream()
                .filter(user -> user.getUserCredentials().getRoles().stream()
                        .map(Role::getName).collect(Collectors.toSet()).contains("Organizer")).collect(Collectors.toSet());

        for (int id : dto.getJury()) {
            User junkies = userRepository.getById(id);
            jury.add(junkies);
        }

        contest.setJury(jury);
        contest.setScored(false);
        contest.setParticipants(new HashSet<>());
        contest.setCoverPhoto(dto.getPhotoUrl());

    }
}
