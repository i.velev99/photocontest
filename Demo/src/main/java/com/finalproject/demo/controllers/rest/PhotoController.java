package com.finalproject.demo.controllers.rest;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.PhotoDto;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.services.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/photos")
public class PhotoController {
    private final PhotoService photoService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public PhotoController(PhotoService photoService,
                           ModelMapper modelMapper,
                           AuthenticationHelper authenticationHelper) {
        this.photoService = photoService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }


    @GetMapping
    public List<Photo> filter(@RequestParam(required = false) Optional<Integer> contestId,
                              @RequestParam(required = false) Optional<Integer> ownerId) {

        PhotoFilterParameters filterParameters = modelMapper.fromParametersListPhoto(contestId, ownerId);
        return photoService.filter(filterParameters);

    }

    @GetMapping("/{id}")
    public Photo getById(@PathVariable int id) {

        return photoService.getById(id);
    }

    @PostMapping
    public Photo create(@RequestHeader HttpHeaders headers,
                        @Valid @RequestBody PhotoDto photoDto) {

        User user = authenticationHelper.tryGetUser(headers);
        Photo photo = modelMapper.fromDto(photoDto);
        photo.setOwner(user);
        photoService.create(photo, user);
        return photo;
    }

    @PutMapping("/{id}")
    public Photo update(@RequestHeader HttpHeaders headers,
                        @PathVariable int id,
                        @Valid @RequestBody PhotoDto photoDto) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Photo photo = modelMapper.fromDto(photoDto, id);
        photoService.update(photo, organizer);
        return photo;
    }

    @DeleteMapping("/{id}")
    public Photo delete(@RequestHeader HttpHeaders headers,
                        @PathVariable int id) {

        User organizer = authenticationHelper.tryGetUser(headers);
        Photo photo = getById(id);
        photoService.delete(photo, organizer);
        return photo;
    }
}
