package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.constants.Constants;
import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.EditUserDto;
import com.finalproject.demo.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static com.finalproject.demo.constants.Constants.PASSWORD_CONFIRM_ERROR_MESSAGE;
import static com.finalproject.demo.constants.Constants.UNAUTHORIZED_ERROR_MESSAGE;

@Controller
@RequestMapping("/profile")
public class UserMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ModelMapper modelMapper;

    @Autowired
    public UserMvcController(AuthenticationHelper authenticationHelper,
                             UserService userService,
                             ModelMapper modelMapper) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    public String showProfilePage(@PathVariable int id, Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            model.addAttribute("currentUser", currentUser);
            User user = userService.getById(id);
            model.addAttribute("user", user);
            return "profile";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/{id}/edit")
    public String showEditProfilePage(@PathVariable int id, HttpSession session, Model model) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to edit his own profile */
            if (currentUser.getId() != (id)) {
                model.addAttribute("error", UNAUTHORIZED_ERROR_MESSAGE);
                return "error";
            }
            User user = userService.getById(id);
            EditUserDto dto = modelMapper.fromUser(user);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("editUserDto", dto);
            return "edit-user";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/{id}/edit")
    public String handleEditUser(Model model,
                                 HttpSession session,
                                 @PathVariable int id,
                                 @Valid @ModelAttribute("editUserDto") EditUserDto dto,
                                 BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            return "edit-user";
        }

        if (!dto.getPassword().equals(dto.getPasswordConfirm())) {
            bindingResult.rejectValue("passwordConfirm", "password_error", PASSWORD_CONFIRM_ERROR_MESSAGE);
            return  "edit-user";
        }

        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to edit his own profile */
            if (currentUser.getId() != id) {
                model.addAttribute("error", UNAUTHORIZED_ERROR_MESSAGE);
                return "error";
            }
            User user = modelMapper.fromDto(dto, id);
            model.addAttribute("user", user);
            model.addAttribute("currentUser", currentUser);
            userService.update(user, currentUser);
            return "redirect:/profile/{id}";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "duplicate_username", e.getMessage());
            return "edit-user";
        }
    }

    @PostMapping("/{id}/delete")
    public String handleDeleteUser(Model model,
                                   HttpSession session,
                                   @PathVariable int id) {

        try {
            User currentUser = authenticationHelper.tryGetUser(session);
            /* Allow access only if the user is trying to delete his own profile */
            if (currentUser.getId() != id) {
                model.addAttribute("error", UNAUTHORIZED_ERROR_MESSAGE);
                return "error";
            }
            User user = userService.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("currentUser", currentUser);
            userService.delete(user, user);
            return "redirect:/";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        } catch (EntityNotFoundException | UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }

    }

}
