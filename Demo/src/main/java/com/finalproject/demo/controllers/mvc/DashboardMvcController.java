package com.finalproject.demo.controllers.mvc;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.exceptions.AuthenticationFailureException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Category;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Type;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.CreateContestDto;
import com.finalproject.demo.repositories.CategoryRepository;
import com.finalproject.demo.repositories.TypeRepository;
import com.finalproject.demo.services.ContestService;
import com.finalproject.demo.services.UserService;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.ORGANIZER_ROLE;

@Controller
@RequestMapping("/dashboard")
public class DashboardMvcController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestService contestService;
    private final ModelMapper modelMapper;
    private final CategoryRepository categoryRepository;
    private final TypeRepository typeRepository;

    @Autowired
    public DashboardMvcController(AuthenticationHelper authenticationHelper,
                                  UserService userService,
                                  ContestService contestService,
                                  ModelMapper modelMapper,
                                  CategoryRepository categoryRepository,
                                  TypeRepository typeRepository) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.contestService = contestService;
        this.modelMapper = modelMapper;
        this.categoryRepository = categoryRepository;
        this.typeRepository = typeRepository;
    }

    @ModelAttribute("categories")
    public List<Category> getAllCategories() {
        return categoryRepository.getAll();
    }

    @ModelAttribute("types")
    public List<Type> getAllTypes() {
        return typeRepository.getAll();
    }


    @ModelAttribute("userJury")
    public List<User> getAllUsersWorthBeingJury() {
        return userService.getAll().stream()
                .filter(user -> user.getPoints() > 150)
                .filter(user -> user.getUserCredentials().isPhotoJunkie()).collect(Collectors.toList());
    }

    @GetMapping
    public String showDashboardPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            model.addAttribute("currentUser", currentUser);
            return "dashboard";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/users")
    public String showUsersPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            model.addAttribute("currentUser", currentUser);
            List<User> users = userService.getAll();
            model.addAttribute("users", users);
            return "all-users";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @GetMapping("/create/contest")
    public String showCreateContestPage(Model model, HttpSession session) {
        try {
            User currentUser = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            model.addAttribute("currentUser", currentUser);
            model.addAttribute("contestDto", new CreateContestDto());
            return "create-contest";
        } catch (AuthenticationFailureException exception) {
            return "redirect:/login";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        }
    }

    @PostMapping("/create/contest")
    public String handleCreateContest(@Valid @ModelAttribute("contestDto") CreateContestDto dto,
                                      BindingResult bindingResult,
                                      Model model,
                                      HttpSession session) {
        if (bindingResult.hasErrors()) {
            return "create-contest";
        }

        try {
            User user = authenticationHelper.verifyAuthorization(session, ORGANIZER_ROLE);
            Contest contest = modelMapper.fromDto(dto);
            contestService.create(contest, user);
            return "redirect:/contests";
        } catch (UnauthorizedOperationException | UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "error";
        } catch (AuthenticationFailureException e) {
            return "redirect:/login";
        }
    }


}
