package com.finalproject.demo.controllers.rest;

import com.finalproject.demo.controllers.AuthenticationHelper;
import com.finalproject.demo.controllers.ModelMapper;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.dto.ReviewDto;
import com.finalproject.demo.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/reviews")
public class ReviewController {

    private final ReviewService reviewService;
    private final ModelMapper modelMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ReviewController(ReviewService reviewService,
                            ModelMapper modelMapper,
                            AuthenticationHelper authenticationHelper) {
        this.reviewService = reviewService;
        this.modelMapper = modelMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Review> getAll() {
        return reviewService.getAll();
    }

    @GetMapping("/{id}")
    public Review getById(@PathVariable int id) {
            return reviewService.getById(id);

    }

    @PostMapping("/{photoId}")
    public Review create(@RequestHeader HttpHeaders headers,
                         @Valid @RequestBody ReviewDto reviewDto,
                         @PathVariable int photoId) {
        User organizer = authenticationHelper.tryGetUser(headers);
        Review review = modelMapper.fromDto(reviewDto);
        review.setCreatedBy(organizer);
        reviewService.create(review, organizer);
        return review;

    }

    @PutMapping("/{photoId}/{id}")
    public Review update(@RequestHeader HttpHeaders headers,
                         @PathVariable int id,
                         @Valid @RequestBody ReviewDto reviewDto,
                         @PathVariable int photoId) {
        User organizer = authenticationHelper.tryGetUser(headers);
        Review review = modelMapper.fromDto(reviewDto, id);
        reviewService.update(review, organizer);
        return review;
    }

    @DeleteMapping("/{photoId}/{id}")
    public Review delete(@RequestHeader HttpHeaders headers,
                         @PathVariable int id,
                         @PathVariable int photoId) {
        User organizer = authenticationHelper.tryGetUser(headers);
        Review review = getById(id);
        reviewService.delete(review, organizer);
        return review;
    }
}
