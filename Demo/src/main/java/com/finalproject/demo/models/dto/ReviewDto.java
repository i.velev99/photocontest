package com.finalproject.demo.models.dto;

import javax.validation.constraints.NotNull;

public class ReviewDto {

    @NotNull
    private int points;

    @NotNull
    private String review;

    @NotNull
    private int photoId;

    public ReviewDto() {
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }
}
