package com.finalproject.demo.models.filterParameters;

import java.util.Optional;

public class PhotoFilterParameters {

    private Optional<Integer> contestId;
    private Optional<Integer> ownerId;

    public PhotoFilterParameters() {
    }

    public PhotoFilterParameters(Optional<Integer> contestId, Optional<Integer> ownerId) {
        this.contestId = contestId;
        this.ownerId = ownerId;
    }

    public Optional<Integer> getContestId() {
        return contestId;
    }

    public void setContestId(Optional<Integer> contestId) {
        this.contestId = contestId;
    }

    public Optional<Integer> getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Optional<Integer> ownerId) {
        this.ownerId = ownerId;
    }
}
