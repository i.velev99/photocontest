package com.finalproject.demo.models;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "photos")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "Title")
    private String title;

    @Column(name = "story")
    private String story;

    @OneToOne
    @JoinColumn(name = "contest_id", referencedColumnName = "id")
    private Contest contest;

    @OneToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    @Column(name = "upload_date")
    private Date uploadDate;

    @Column(name = "photo")
    private String photoUrl;

    @Column(name = "place")
    private int place;

    @Column(name = "score")
    private double score;

    public Photo() {
    }

    public Photo(int id,
                 String title,
                 String story,
                 Contest contest,
                 User owner,
                 Date uploadDate,
                 String photoUrl) {
        this.id = id;
        this.title = title;
        this.story = story;
        this.contest = contest;
        this.owner = owner;
        this.uploadDate = uploadDate;
        this.photoUrl = photoUrl;
        this.place = 0;
        this.score = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Date getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(Date uploadDate) {
        this.uploadDate = uploadDate;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    public void addToScore(int pointsToAdd) {
        double points = getScore();
        points += pointsToAdd;
        setScore(points);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Photo photo = (Photo) o;

        if (id != photo.id) return false;
        if (place != photo.place) return false;
        if (Double.compare(photo.score, score) != 0) return false;
        if (title != null ? !title.equals(photo.title) : photo.title != null) return false;
        if (story != null ? !story.equals(photo.story) : photo.story != null) return false;
        if (contest != null ? !contest.equals(photo.contest) : photo.contest != null) return false;
        if (owner != null ? !owner.equals(photo.owner) : photo.owner != null) return false;
        if (uploadDate != null ? !uploadDate.equals(photo.uploadDate) : photo.uploadDate != null) return false;
        return photoUrl != null ? photoUrl.equals(photo.photoUrl) : photo.photoUrl == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (story != null ? story.hashCode() : 0);
        result = 31 * result + (contest != null ? contest.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (uploadDate != null ? uploadDate.hashCode() : 0);
        result = 31 * result + (photoUrl != null ? photoUrl.hashCode() : 0);
        result = 31 * result + place;
        temp = Double.doubleToLongBits(score);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

}
