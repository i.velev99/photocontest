package com.finalproject.demo.models.filterParameters;

import java.util.Optional;

public class ContestFilterParameters {

    private Optional<Integer> categoryId;
    private Optional<Integer> typeId;

    public ContestFilterParameters() {
    }

    public ContestFilterParameters(Optional<Integer> categoryId, Optional<Integer> typeId) {
        this.categoryId = categoryId;
        this.typeId = typeId;
    }

    public Optional<Integer> getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Optional<Integer> categoryId) {
        this.categoryId = categoryId;
    }

    public Optional<Integer> getTypeId() {
        return typeId;
    }

    public void setTypeId(Optional<Integer> typeId) {
        this.typeId = typeId;
    }
}
