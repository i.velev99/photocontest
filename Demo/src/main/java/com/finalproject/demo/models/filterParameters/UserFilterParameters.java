package com.finalproject.demo.models.filterParameters;

import java.util.Optional;

public class UserFilterParameters {

    private Optional<String> firstName;
    private Optional<String> lastName;
    private Optional<String> username;

    public UserFilterParameters() {
    }

    public UserFilterParameters(Optional<String> firstName, Optional<String> lastName, Optional<String> username) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
    }

    public Optional<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(Optional<String> firstName) {
        this.firstName = firstName;
    }

    public Optional<String> getLastName() {
        return lastName;
    }

    public void setLastName(Optional<String> lastName) {
        this.lastName = lastName;
    }

    public Optional<String> getUsername() {
        return username;
    }

    public void setUsername(Optional<String> username) {
        this.username = username;
    }
}
