package com.finalproject.demo.models.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class RegisterDto extends LoginDto {

    @NotEmpty(message = "Password confirmation can't be empty")
    private String passwordConfirm;

    @NotEmpty(message = "First name can't be empty")
    @Size(min = 2 , max = 20 , message = "First Name should be between 2 and  20")
    private String firstName;

    @NotEmpty(message = "Last name can't be empty")
    @Size(min = 2 , max = 20 , message = "Last Name should be between 2 and 20")
    private String lastName;

    public RegisterDto() {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
