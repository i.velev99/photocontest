package com.finalproject.demo.models.dto;

import javax.validation.constraints.NotNull;

public class PhotoDto {

    @NotNull
    private String title;

    @NotNull
    private String story;

    @NotNull
    private int contestId;

    @NotNull
    private String photoUrl;

    public PhotoDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

}
