package com.finalproject.demo.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserCredentialsDto {

    @NotNull(message = "Username should not be null")
    @Size(min = 4, max = 20, message = "Username should be between 4 and 20")
    private String username;

    @NotNull(message = "Password should not be null")
    @Size(min = 8, max = 100, message = "Password should be between 8 and 100")
    private String password;

    public UserCredentialsDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
