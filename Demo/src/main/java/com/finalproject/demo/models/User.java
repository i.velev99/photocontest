package com.finalproject.demo.models;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @OneToOne
    @JoinColumn(name = "credentials_id", referencedColumnName = "username")
    private UserCredentials userCredentials;

    @Column(name = "points")
    private int points;

    public User() {
    }

    public User(int id, String firstName, String lastName, UserCredentials userCredentials) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userCredentials = userCredentials;
        this.points = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserCredentials getUserCredentials() {
        return userCredentials;
    }

    public void setUserCredentials(UserCredentials userCredentials) {
        this.userCredentials = userCredentials;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void addToPoints(int pointsToAdd) {
        int points = getPoints();
        points += pointsToAdd;
        setPoints(points);
    }

    public String getRank(int points) {
        if (points <= 50) {
            return "Junkie";
        }

        if (points <= 150) {
            return "Enthusiast";
        }

        if (points <= 1000) {
            return "Master";
        }

        return "Wise and Benevolent Photo Dictator";
    }

    public int getNextRankPoints(int points) {
        if (points <= 50) {
            return 50;
        }


        //Enthusiast
        if (points <= 150) {
            return 150;
        }

        //Master
        if (points <= 1000) {
            return 1000;
        }

            return 100000;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (points != user.points) return false;
        if (!firstName.equals(user.firstName)) return false;
        if (!lastName.equals(user.lastName)) return false;
        return userCredentials.equals(user.userCredentials);
    }

    @Override
    public int hashCode() {
        int result = firstName.hashCode();
        result = 31 * result + lastName.hashCode();
        result = 31 * result + userCredentials.hashCode();
        return result;
    }
}
