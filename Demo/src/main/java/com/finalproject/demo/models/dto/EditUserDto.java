package com.finalproject.demo.models.dto;

import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Role;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

public class EditUserDto {

    public static final String PASSWORD_SIZE_ERROR_MESSAGE = "Password should be between 8 and 24";
    public static final String PASSWORD_NOT_NULL_ERROR_MESSAGE = "Password confirmation can't be empty";
    public static final String LAST_NAME_NULL_ERROR_MESSAGE = "Last name can't be empty";
    public static final String FIRST_NAME_NULL_ERROR_MESSAGE = "First Name can't be empty";
    public static final String FIRST_NAME_SIZE_ERROR_MESSAGE = "First Name should be between 2 and  20";
    public static final String LAST_NAME_SIZE_ERROR_MESSAGE = "Last Name should be between 2 and  20";
    private int id;

    @NotEmpty(message = PASSWORD_NOT_NULL_ERROR_MESSAGE)
    @Size(min = 8 , max = 24 , message = PASSWORD_SIZE_ERROR_MESSAGE)
    private String password;

    @NotEmpty(message = PASSWORD_NOT_NULL_ERROR_MESSAGE)
    @Size(min = 8 , max = 24 , message = PASSWORD_SIZE_ERROR_MESSAGE)
    private String passwordConfirm;

    @NotEmpty(message = FIRST_NAME_NULL_ERROR_MESSAGE)
    @Size(min = 2 , max = 20 , message = FIRST_NAME_SIZE_ERROR_MESSAGE)
    private String firstName;

    @NotEmpty(message = LAST_NAME_NULL_ERROR_MESSAGE)
    @Size(min = 2 , max = 20 , message = LAST_NAME_SIZE_ERROR_MESSAGE)
    private String lastName;

    private Set<Role> roles;

    public EditUserDto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }
}
