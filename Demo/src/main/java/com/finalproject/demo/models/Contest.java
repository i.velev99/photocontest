package com.finalproject.demo.models;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest implements Comparable<Contest> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @OneToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private Category category;

    @OneToOne
    @JoinColumn(name = "type_id", referencedColumnName = "id")
    private Type type;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "first_phase_final_date")
    private Date firstPhaseFinalDate;

    @Column(name = "second_phase_final_date")
    private Date secondPhaseFinalDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "contest_juries",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> jury;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "contest_participants",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id"))
    private Set<User> participants;

    @Column(name = "is_scored")
    private boolean isScored;

    @Column(name = "cover_photo")
    private String coverPhoto;

    public Contest() {
    }

    public Contest(int id,
                   String title,
                   Category category,
                   Type type,
                   Date startDate,
                   Date firstPhaseFinalDate,
                   Date secondPhaseFinalDate,
                   String coverPhoto) {
        this.id = id;
        this.title = title;
        this.category = category;
        this.type = type;
        this.startDate = startDate;
        this.firstPhaseFinalDate = firstPhaseFinalDate;
        this.secondPhaseFinalDate = secondPhaseFinalDate;
        this.coverPhoto = coverPhoto;
        this.isScored = false;
        this.jury = new HashSet<>();
        this.participants = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFirstPhaseFinalDate() {
        return firstPhaseFinalDate;
    }

    public void setFirstPhaseFinalDate(Date firstPhaseFinalDate) {
        this.firstPhaseFinalDate = firstPhaseFinalDate;
    }

    public Date getSecondPhaseFinalDate() {
        return secondPhaseFinalDate;
    }

    public void setSecondPhaseFinalDate(Date secondPhaseFinalDate) {
        this.secondPhaseFinalDate = secondPhaseFinalDate;
    }

    public Set<User> getJury() {
        return jury;
    }

    public void setJury(Set<User> jury) {
        this.jury = jury;
    }

    public Set<User> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<User> participants) {
        this.participants = participants;
    }

    public boolean isScored() {
        return isScored;
    }

    public void setScored(boolean scored) {
        isScored = scored;
    }

    public String getCoverPhoto() {
        return coverPhoto;
    }

    public void setCoverPhoto(String coverPhoto) {
        this.coverPhoto = coverPhoto;
    }

    public String getPhase() {

        if (Date.from(Instant.now()).before(startDate)) {
            return "Not Started Yet";
        }

        if (Date.from(Instant.now()).after(startDate) && Date.from(Instant.now()).before(getFirstPhaseFinalDate())) {
            return "Phase 1";
        }

        if (Date.from(Instant.now()).after(getFirstPhaseFinalDate()) && Date.from(Instant.now()).before(getSecondPhaseFinalDate())) {
            return "Phase 2";
        }

        return "Finished";

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contest contest = (Contest) o;

        if (id != contest.id) return false;
        if (isScored != contest.isScored) return false;
        if (title != null ? !title.equals(contest.title) : contest.title != null) return false;
        if (category != null ? !category.equals(contest.category) : contest.category != null) return false;
        if (type != null ? !type.equals(contest.type) : contest.type != null) return false;
        if (startDate != null ? !startDate.equals(contest.startDate) : contest.startDate != null) return false;
        if (firstPhaseFinalDate != null ? !firstPhaseFinalDate.equals(contest.firstPhaseFinalDate) : contest.firstPhaseFinalDate != null)
            return false;
        if (secondPhaseFinalDate != null ? !secondPhaseFinalDate.equals(contest.secondPhaseFinalDate) : contest.secondPhaseFinalDate != null)
            return false;
        if (jury != null ? !jury.equals(contest.jury) : contest.jury != null) return false;
        if (participants != null ? !participants.equals(contest.participants) : contest.participants != null)
            return false;
        return coverPhoto != null ? coverPhoto.equals(contest.coverPhoto) : contest.coverPhoto == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (category != null ? category.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (firstPhaseFinalDate != null ? firstPhaseFinalDate.hashCode() : 0);
        result = 31 * result + (secondPhaseFinalDate != null ? secondPhaseFinalDate.hashCode() : 0);
        result = 31 * result + (jury != null ? jury.hashCode() : 0);
        result = 31 * result + (participants != null ? participants.hashCode() : 0);
        result = 31 * result + (isScored ? 1 : 0);
        result = 31 * result + (coverPhoto != null ? coverPhoto.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Contest o) {
        return getStartDate().compareTo(o.getStartDate());
    }

    public long daysBetweenDates(Date firstDate, Date secondDate) {

        return 1 + ChronoUnit.DAYS.between(firstDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime(),
                        secondDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }
}
