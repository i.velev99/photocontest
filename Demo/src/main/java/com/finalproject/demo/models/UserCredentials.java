package com.finalproject.demo.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Table
@Entity(name = "user_credentials")
public class UserCredentials {

    @Id
    @Column(name = "username")
    @NotNull
    private String username;

    @Column(name = "password")
    @NotNull
    @JsonIgnore
    private String password;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "credentials_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;

    public UserCredentials() {
    }

    public UserCredentials(@NotNull String username, @NotNull String password) {
        this.username = username;
        this.password = password;
        roles = new HashSet<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public boolean isPhotoJunkie() {
        return roles.stream().map(Role::getName).collect(Collectors.toSet()).contains("Photo Junkie");
    }

    public boolean isOrganizer() {
        return roles.stream().map(Role::getName).collect(Collectors.toSet()).contains("Organizer");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserCredentials that = (UserCredentials) o;

        if (!username.equals(that.username)) return false;
        if (!password.equals(that.password)) return false;
        return roles.equals(that.roles);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + password.hashCode();
        result = 31 * result + roles.hashCode();
        return result;
    }
}
