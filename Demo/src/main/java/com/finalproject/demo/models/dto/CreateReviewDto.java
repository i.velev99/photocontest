package com.finalproject.demo.models.dto;

import com.finalproject.demo.models.User;

import javax.validation.constraints.NotNull;

public class CreateReviewDto {
    @NotNull
    private int points;

    @NotNull
    private String review;

    @NotNull
    private int photoId;

    private boolean isInappropriate;

    public CreateReviewDto() {
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public String getReview() {
        return review;
    }

    public void setReview(String review) {
        this.review = review;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public boolean isInappropriate() {
        return isInappropriate;
    }

    public void setInappropriate(boolean inappropriate) {
        isInappropriate = inappropriate;
    }
}

