package com.finalproject.demo.models.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Set;

public class ContestDto {

    @NotNull
    private String title;

    @NotNull
    private int categoryId;

    @NotNull
    private int typeId;

    @NotNull(message = "Date cannot be null")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date startDate;

    @NotNull
    @Min(1)
    @Max(31)
    private int firstPhaseTimeLimit;

    @NotNull
    @Min(1)
    @Max(24)
    private int secondPhaseTimeLimit;

    private Set<String> jury;

    private Set<Integer> participants;

    @NotNull
    private String photoUrl;

    public ContestDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getFirstPhaseTimeLimit() {
        return firstPhaseTimeLimit;
    }

    public void setFirstPhaseTimeLimit(int firstPhaseTimeLimit) {
        this.firstPhaseTimeLimit = firstPhaseTimeLimit;
    }

    public int getSecondPhaseTimeLimit() {
        return secondPhaseTimeLimit;
    }

    public void setSecondPhaseTimeLimit(int secondPhaseTimeLimit) {
        this.secondPhaseTimeLimit = secondPhaseTimeLimit;
    }

    public Set<String> getJury() {
        return jury;
    }

    public void setJury(Set<String> jury) {
        this.jury = jury;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<Integer> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Integer> participants) {
        this.participants = participants;
    }
}
