package com.finalproject.demo.utils;

import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Role;
import com.finalproject.demo.models.User;

import java.util.Set;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.USER_AUTHORIZATION_ERROR_MESSAGE;

public class Utils {

    public static void checkAuthorization(User user, String role) {
        Set<String> organizerRoles = user.getUserCredentials().getRoles().
                stream().
                map(Role::getName).
                collect(Collectors.toSet());

        if(!organizerRoles.contains(role)) {
            throw new UnauthorizedOperationException(USER_AUTHORIZATION_ERROR_MESSAGE);
        }
    }

}
