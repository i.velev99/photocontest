package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class ContestRepositoryImpl implements ContestRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Contest> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest ", Contest.class);
            return query.getResultList();
        }
    }

    @Override
    public List<Contest> filter(ContestFilterParameters filterParameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Contest where 1=1";
            var filters = new ArrayList<String>();

            filterParameters.getCategoryId().ifPresent(q -> filters.add(" and category.id = :categoryId "));
            filterParameters.getTypeId().ifPresent(q -> filters.add(" and type.id = :typeId "));

            queryString = queryString + String.join("", filters);

            Query<Contest> query = session.createQuery(queryString, Contest.class);
            filterParameters.getCategoryId().ifPresent(first -> query.setParameter("categoryId", first));
            filterParameters.getTypeId().ifPresent(last -> query.setParameter("typeId", last));
            return query.list();
        }
    }

    public List<Contest> filterByPhase(List<Contest> contests, String phase) {
        return contests.stream()
                .filter(contest -> contest.getPhase().equalsIgnoreCase(phase))
                .collect(Collectors.toList());

    }

    @Override
    public Contest getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Contest where id = :id";
            Query<Contest> query = session.createQuery(queryString, Contest.class);
            query.setParameter("id", id);
            List<Contest> contests = query.list();
            if (contests.size() == 0) {
                throw new EntityNotFoundException("Contest", id);
            }
            return contests.get(0);
        }
    }

    @Override
    public void join(Contest contest, User user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(contest);
            session.update(user);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void create(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(contest);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(contest);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(contest);
            transaction.commit();
        }
    }
}
