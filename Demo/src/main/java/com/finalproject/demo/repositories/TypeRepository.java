package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Type;

import java.util.List;

public interface TypeRepository {
    List<Type> getAll();

    Type getById(int id);
}
