package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ReviewRepositoryImpl implements ReviewRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Review> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review ", Review.class);
            return query.getResultList();
        }
    }

    @Override
    public Review getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Review where id = :id";
            Query<Review> query = session.createQuery(queryString, Review.class);
            query.setParameter("id", id);
            List<Review> reviews = query.list();
            if (reviews.size() == 0) {
                throw new EntityNotFoundException("Review", id);
            }
            return reviews.get(0);
        }
    }

    @Override
    @Transactional
    public void create(Review review, Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(photo);
            session.save(review);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Review review, Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(photo);
            session.update(review);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Review review, Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(photo);
            session.delete(review);
            transaction.commit();
        }
    }
}
