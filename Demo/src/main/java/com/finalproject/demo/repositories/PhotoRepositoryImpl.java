package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PhotoRepositoryImpl implements PhotoRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Photo> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Photo> query = session.createQuery("from Photo ", Photo.class);
            return query.getResultList();
        }
    }


    @Override
    public List<Photo> filter(PhotoFilterParameters filterParameters) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Photo where 1=1 ";
            var filters = new ArrayList<String>();

            filterParameters.getContestId().ifPresent(q -> filters.add(" and contest.id = :contestId "));
            filterParameters.getOwnerId().ifPresent(q -> filters.add(" and owner.id = :ownerId "));

            queryString = queryString + String.join("", filters);

            Query<Photo> query = session.createQuery(queryString, Photo.class);
            filterParameters.getContestId().ifPresent(first -> query.setParameter("contestId", first));
            filterParameters.getOwnerId().ifPresent(last -> query.setParameter("ownerId", last));
            return query.list();
        }
    }

    @Override
    public Photo getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Photo where id = :id";
            Query<Photo> query = session.createQuery(queryString, Photo.class);
            query.setParameter("id", id);
            List<Photo> photos = query.list();
            if (photos.size() == 0) {
                throw new EntityNotFoundException("Photo", id);
            }
            return photos.get(0);
        }
    }

    @Override
    @Transactional
    public void create(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(photo);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(photo);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Photo photo) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(photo);
            transaction.commit();
        }
    }
}
