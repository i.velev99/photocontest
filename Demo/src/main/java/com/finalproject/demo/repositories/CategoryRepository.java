package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Category;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;

import java.util.List;

public interface CategoryRepository {
    List<Category> getAll();

    Category getById(int id);
}
