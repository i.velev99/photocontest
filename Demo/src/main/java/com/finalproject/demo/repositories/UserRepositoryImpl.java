package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User ", User.class);
            return query.list();
        }
    }


    @Override
    public List<User> filter(UserFilterParameters ufp) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from User where 1=1 ";
            var filters = new ArrayList<String>();

            ufp.getUsername().ifPresent(q -> filters.add(" and firstName like concat('%',:firstname,'%') "));
            ufp.getUsername().ifPresent(q -> filters.add(" and lastName like concat('%',:lastname,'%') "));
            ufp.getUsername().ifPresent(q -> filters.add(" and userCredentials.username like concat('%',:username,'%') "));

            queryString = queryString + String.join("", filters);
            queryString = queryString + " order by points desc ";

            Query<User> query = session.createQuery(queryString, User.class);
            ufp.getUsername().ifPresent(first -> query.setParameter("firstname", first));
            ufp.getLastName().ifPresent(last -> query.setParameter("lastname", last));
            ufp.getUsername().ifPresent(username -> query.setParameter("username", username));
            return query.list();
        }
    }


    public int pointsUntilNextRank(User user) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "select sum (:points - u.points) From User as u " +
                    "where u.id = :userId";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("points", user.getNextRankPoints(user.getPoints()));
            query.setParameter("userId", user.getId());
            query.getSingleResult();
            return query.getFirstResult();
        }
    }

    @Override
    public User getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From User where id = :id";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("id", id);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", id);
            }
            return users.get(0);
        }
    }

    @Override
    public User getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From User where userCredentials.username = :username";
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter("username", username);
            List<User> users = query.list();
            if (users.size() == 0) {
                throw new EntityNotFoundException("User", "username", username);
            }
            return users.get(0);
        }
    }

    @Override
    @Transactional
    public void create(User user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(user.getUserCredentials());
            session.save(user);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(User user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(user.getUserCredentials());
            session.update(user);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(User user) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(user);
            session.delete(user.getUserCredentials());
            transaction.commit();
        }
    }

}
