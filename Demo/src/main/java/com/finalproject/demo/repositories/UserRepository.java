package com.finalproject.demo.repositories;

import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    public int pointsUntilNextRank(User user);

    @Transactional
    void create(User user);

    @Transactional
    void update(User user);

    @Transactional
    void delete(User user);

    List<User> filter(UserFilterParameters ufp);
}
