package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.Role;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RoleRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Role> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Role> query = session.createQuery("from Role ", Role.class);
            return query.getResultList();
        }
    }

    @Override
    public Role getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Role where id = :id";
            Query<Role> query = session.createQuery(queryString, Role.class);
            query.setParameter("id", id);
            List<Role> roles = query.list();
            if (roles.size() == 0) {
                throw new EntityNotFoundException("Role", id);
            }
            return roles.get(0);
        }
    }
    @Override
    public Role getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Role where name = :name";
            Query<Role> query = session.createQuery(queryString, Role.class);
            query.setParameter("name", name);
            List<Role> roles = query.list();
            if (roles.size() == 0) {
                throw new EntityNotFoundException("Role", "name", name);
            }
            return roles.get(0);
        }
    }
}
