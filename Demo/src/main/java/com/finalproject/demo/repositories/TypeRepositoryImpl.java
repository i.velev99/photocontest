package com.finalproject.demo.repositories;

import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.models.Category;
import com.finalproject.demo.models.Type;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TypeRepositoryImpl implements TypeRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public TypeRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Type> getAll() {
        try(Session session = sessionFactory.openSession()) {
            Query<Type> query = session.createQuery("from Type ", Type.class);
            return query.getResultList();
        }
    }

    @Override
    public Type getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Type where id = :id";
            Query<Type> query = session.createQuery(queryString, Type.class);
            query.setParameter("id", id);
            List<Type> types = query.list();
            if (types.size() == 0) {
                throw new EntityNotFoundException("Type", id);
            }
            return types.get(0);
        }
    }

}
