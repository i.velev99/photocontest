package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;

import java.util.List;

public interface ReviewRepository {
    List<Review> getAll();

    Review getById(int id);

    void create(Review review, Photo photo);

    void update(Review review, Photo photo);

    void delete(Review review, Photo photo);
}
