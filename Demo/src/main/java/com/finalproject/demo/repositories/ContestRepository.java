package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;

import java.util.List;

public interface ContestRepository {

    List<Contest> getAll();

    List<Contest> filter(ContestFilterParameters filterParameters);

    List<Contest> filterByPhase(List<Contest> contests, String phase);

    Contest getById(int id);

    void create(Contest contest);

    void update(Contest contest);

    void delete(Contest contest);

    void join(Contest contest, User user);
}
