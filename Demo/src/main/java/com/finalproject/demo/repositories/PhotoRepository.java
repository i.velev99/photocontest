package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;

import java.util.List;

public interface PhotoRepository {
    List<Photo> getAll();

    List<Photo> filter(PhotoFilterParameters filterParameters);

    Photo getById(int id);

    void create(Photo photo);

    void update(Photo photo);

    void delete(Photo photo);
}
