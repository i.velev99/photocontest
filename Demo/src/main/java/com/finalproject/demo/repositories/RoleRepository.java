package com.finalproject.demo.repositories;

import com.finalproject.demo.models.Role;

import java.util.List;

public interface RoleRepository {
    List<Role> getAll();

    Role getById(int id);

    Role getByName(String name);
}
