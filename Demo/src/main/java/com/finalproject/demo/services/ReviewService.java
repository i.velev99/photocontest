package com.finalproject.demo.services;

import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;

import java.util.List;

public interface ReviewService {
    List<Review> getAll();

    Review getById(int id);

    void create(Review review, User organizer);

    void update(Review review, User organizer);

    void delete(Review review, User organizer);
}
