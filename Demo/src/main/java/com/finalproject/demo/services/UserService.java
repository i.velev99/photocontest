package com.finalproject.demo.services;

import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    public int pointsUntilNextRank(User user);

    void create(User user);

    void update(User user, User creator);

    void delete(User user, User creator);

    List<User> filter(UserFilterParameters ufp);
}
