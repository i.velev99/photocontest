package com.finalproject.demo.services;

import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;

import java.util.List;

public interface ContestService {
    List<Contest> getAll();

    List<Contest> filter(ContestFilterParameters filterParameters);

    List<Contest> filterByPhase(List<Contest> contests, String phase);

    Contest getById(int id);

    void join(Contest contest, User user, boolean isInvited);

    void create(Contest contest, User organizer);

    void update(Contest contest, User organizer);

    void delete(Contest contest, User organizer);

}
