package com.finalproject.demo.services;

import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.repositories.PhotoRepository;
import com.finalproject.demo.repositories.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.*;

@Service
public class ReviewServiceImpl implements ReviewService {

    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImpl(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<Review> getAll() {
        return reviewRepository.getAll();
    }

    @Override
    public Review getById(int id) {
        return reviewRepository.getById(id);
    }

    @Override
    public void create(Review review, User organizer) {
        Photo photo = review.getPhoto();
        checkJuryAndPhase(photo, organizer);
        checkReviewed(photo,organizer);
        photo.addToScore(review.getPoints());
        reviewRepository.create(review, photo);
    }

    @Override
    public void update(Review review, User organizer) {
        Photo photo = review.getPhoto();
        checkJuryAndPhase(photo, organizer);
        checkCreator(review,organizer);
        reviewRepository.update(review,photo);
    }

    @Override
    public void delete(Review review, User organizer) {
        Photo photo = review.getPhoto();
        checkJuryAndPhase(photo, organizer);
        checkCreator(review, organizer);
        reviewRepository.delete(review,photo);
    }

    private void checkJuryAndPhase(Photo photo,User organizer) {
        Contest contest = photo.getContest();

        if (!contest.getPhase().equalsIgnoreCase(PHASE_TWO)) {
            throw new UnsupportedOperationException(CANNOT_REVIEW);
        }

        if (!contest.getJury().contains(organizer)) {
            throw new UnauthorizedOperationException(String.format(USER_NOT_JURY, organizer));
        }
    }

    private void checkCreator(Review review,User organizer) {
        if(review.getCreatedBy().getId() != organizer.getId()) {
            throw new UnauthorizedOperationException(USER_NOT_CREATOR);
        }
    }

    public void checkReviewed(Photo photo, User user) {
        List<Review> reviews = reviewRepository.getAll()
                .stream()
                .filter(review -> review.getPhoto().getId() == photo.getId())
                .filter(review -> review.getCreatedBy().getId() == user.getId())
                .collect(Collectors.toList());

        if (reviews.size() > 0) {
            throw new DuplicateEntityException(USER_REVIEWED_PHOTO_ERROR_MESSAGE);
        }
    }

}
