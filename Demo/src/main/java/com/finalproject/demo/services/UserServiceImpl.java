package com.finalproject.demo.services;

import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.exceptions.EntityNotFoundException;
import com.finalproject.demo.exceptions.UnauthorizedOperationException;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.UserFilterParameters;
import com.finalproject.demo.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.finalproject.demo.constants.Constants.USER_NOT_CREATOR;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public List<User> filter(UserFilterParameters ufp) {
        return userRepository.filter(ufp);
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(User user) {
        checkDuplicateUsername(user);
        userRepository.create(user);
    }

    @Override
    public void update(User user, User creator) {

        checkCreator(user, creator);

        checkDuplicateUsername(user);

        userRepository.update(user);
    }

    private void checkDuplicateUsername(User user) {
        boolean duplicateExists = false;

        try {
            User result = userRepository.getByUsername(user.getUserCredentials().getUsername());
            if (user.getId() != result.getId()) {
                duplicateExists = true;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUserCredentials().getUsername());
        }
    }

    @Override
    public int pointsUntilNextRank(User user) {
        return userRepository.pointsUntilNextRank(user);
    }

    @Override
    public void delete(User user, User creator) {
        checkCreator(user, creator);
        userRepository.delete(user);
    }

    private void checkCreator(User user, User creator) {
        if (creator.getId() != user.getId()) {
            throw new UnauthorizedOperationException(USER_NOT_CREATOR);
        }
    }

}
