package com.finalproject.demo.services;

import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.Review;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.*;

@Service
public class Scoring implements Runnable {

    private final ContestRepository contestRepository;
    private final PhotoRepository photoRepository;
    private final UserRepository userRepository;
    private final ReviewRepository reviewRepository;

    @Autowired
    public Scoring(ContestRepository contestRepository,
                   PhotoRepository photoRepository,
                   UserRepository userRepository,
                   ReviewRepository reviewRepository) {
        this.contestRepository = contestRepository;
        this.photoRepository = photoRepository;
        this.userRepository = userRepository;
        this.reviewRepository = reviewRepository;
    }

    public void manualScoring() {

        List<Contest> finished = contestRepository.filterByPhase(contestRepository.getAll(), FINISHED_PHASE)
                .stream().filter(contest -> !contest.isScored()).collect(Collectors.toList());

        if (finished == null || finished.size() == 0) {
            return;
        }

        finished.forEach(this::score);
    }

    private void score(Contest c) {

        List<Photo> contestPhotos = photoRepository.filter(new PhotoFilterParameters(Optional.of(c.getId()), Optional.empty()));
        if (contestPhotos.size() == 0) {
            return;
        }
        int jurySize = c.getJury().size();
        contestPhotos.forEach(photo -> addPointsAuto(photo, jurySize));
        List<Photo> sortedPhotos = contestPhotos.stream().sorted(Comparator.comparingDouble(Photo::getScore).reversed()).collect(Collectors.toList());

        List<Photo> first = getLeads(sortedPhotos);
        first.forEach(photo -> photo.setPlace(FIRST));
        first.forEach(photoRepository::update);
        sortedPhotos.removeAll(first);

        List<Photo> seconds = getLeads(sortedPhotos);
        seconds.forEach(photo -> photo.setPlace(SECOND));
        seconds.forEach(photoRepository::update);
        sortedPhotos.removeAll(seconds);

        List<Photo> thirds = getLeads(sortedPhotos);
        thirds.forEach(photo -> photo.setPlace(THIRD));
        thirds.forEach(photoRepository::update);
        sortedPhotos.removeAll(thirds);

        scoreFirst(first, seconds);
        scoreOthers(seconds, ALONE_POINTS_SECOND, SHARE_POINTS_SECOND);
        scoreOthers(thirds,ALONE_POINTS_THIRD, SHARE_POINTS_THIRD);

        sortedPhotos.forEach(photo -> photo.setPlace(FOURTH));
        sortedPhotos.forEach(photoRepository::update);

        c.setScored(true);
        contestRepository.update(c);
    }

    private void scoreFirst(List<Photo> first, List<Photo> seconds) {

        if (seconds.size() == 0 || first.get(0).getScore() >= seconds.get(0).getScore() * 2) {
            first.forEach(photo -> setUserPoints(photo.getOwner(), FIRST_PLACE_POINTS_DOUBLE));

        } else if (first.size() == 1) {
            setUserPoints(first.get(0).getOwner(), FIRST_PLACE_POINTS);

        } else {
            first.forEach(photo -> setUserPoints(photo.getOwner(),FIRST_PLACE_POINTS_SHARED));
        }
    }

    private void setUserPoints(User user,int points) {
        user.addToPoints(points);
        userRepository.update(user);
    }

    private void scoreOthers(List<Photo> photos, int alone, int share) {
        if (photos.size() == 1) {
            setUserPoints(photos.get(0).getOwner(),alone);
        } else {
            photos.forEach(photo -> setUserPoints(photo.getOwner(),share));
        }
    }

    private List<Photo> getLeads(List<Photo> sortedPhotos) {

        if (sortedPhotos.size() == 0) {
            return new ArrayList<>();
        }

        List<Photo> first = new ArrayList<>();
        double firstPhotoScore = sortedPhotos.get(0).getScore();

        for (Photo photo : sortedPhotos) {
            if (photo.getScore() == firstPhotoScore) {
                first.add(photo);
            } else {
                break;
            }
        }
        return first;
    }

    private void addPointsAuto(Photo photo, int jurySize) {
        Set<Review> photoReviews= reviewRepository.getAll()
                .stream()
                .filter(review -> review.getPhoto().getId() == photo.getId())
                .collect(Collectors.toSet());
        int notScored = jurySize - photoReviews.size();
        double finalScore = notScored * 3 + photo.getScore();
        photo.setScore(finalScore);
    }

    @Override
    @Scheduled(fixedRate = 1_800_000)
    public void run() {
        manualScoring();
    }
}
