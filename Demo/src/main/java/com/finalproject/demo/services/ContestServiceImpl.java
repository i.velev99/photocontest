package com.finalproject.demo.services;

import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.ContestFilterParameters;
import com.finalproject.demo.repositories.ContestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.finalproject.demo.constants.Constants.*;
import static com.finalproject.demo.utils.Utils.checkAuthorization;

@Service
public class ContestServiceImpl implements ContestService {

    private final ContestRepository contestRepository;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository) {
        this.contestRepository = contestRepository;
    }

    @Override
    public List<Contest> getAll() {
        return contestRepository.getAll();
    }

    @Override
    public List<Contest> filter(ContestFilterParameters filterParameters) {
        return contestRepository.filter(filterParameters);
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    public List<Contest> filterByPhase(List<Contest> contests, String phase) {
        return contestRepository.filterByPhase(contests,phase);
    }

    @Override
    public void join(Contest contest, User user, boolean isInvited) {
        checkAuthorization(user,PHOTO_JUNKIE_ROLE);

        if(!contest.getPhase().equalsIgnoreCase(PHASE_ONE)) {
            throw new UnsupportedOperationException(CONTEST_JOIN_ERROR_MESSAGE);
        }

        if(contest.getParticipants().contains(user)) {
            throw new DuplicateEntityException(USER_IS_ALREADY_PARTICIPATING_ERROR_MESSAGE);
        }

        if(contest.getJury().contains(user)) {
            throw new UnsupportedOperationException(USER_JURY_ERROR_MESSAGE);
        }
        addPoints(user, isInvited);
        Set<User> participants = new HashSet<>(contest.getParticipants());
        participants.add(user);
        contest.setParticipants(participants);
        contestRepository.join(contest, user);
    }

    @Override
    public void create(Contest contest, User organizer) {
        checkAuthorization(organizer, ORGANIZER_ROLE);
        if(contest.getStartDate().before(Date.from(Instant.now()))) {
            throw new UnsupportedOperationException(CONTEST_MUST_BE_IN_FUTURE_ERROR_MESSAGE);
        }
        removeUsersFromJury(contest);
        contestRepository.create(contest);
    }

    @Override
    public void update(Contest contest, User organizer) {
        checkAuthorization(organizer, ORGANIZER_ROLE);
        if(contest.getStartDate().before(Date.from(Instant.now()))) {
            throw new UnsupportedOperationException(CONTEST_MUST_BE_IN_FUTURE_ERROR_MESSAGE);
        }
        contestRepository.update(contest);
    }

    @Override
    public void delete(Contest contest, User organizer) {
        checkAuthorization(organizer, ORGANIZER_ROLE);
        contestRepository.delete(contest);
    }

    private void addPoints(User user, boolean isInvited) {
        if (isInvited) {
            user.addToPoints(3);
        } else {
            user.addToPoints(1);
        }
    }

    private void removeUsersFromJury(Contest contest) {
        for (User jury : contest.getJury()) {
            if(jury.getUserCredentials().isPhotoJunkie()) {
                if(!jury.getRank(jury.getPoints()).equals(WISE_AND_BENEVOLENT_PHOTO_DICTATOR_RANK)
                        && !jury.getRank(jury.getPoints()).equals(MASTER_RANK)) {
                    contest.getJury().remove(jury);
                }
            }
        }
    }

}
