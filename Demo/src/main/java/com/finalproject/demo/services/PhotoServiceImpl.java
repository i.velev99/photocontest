package com.finalproject.demo.services;

import com.finalproject.demo.exceptions.DuplicateEntityException;
import com.finalproject.demo.models.Contest;
import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import com.finalproject.demo.repositories.PhotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import static com.finalproject.demo.constants.Constants.*;
import static com.finalproject.demo.utils.Utils.checkAuthorization;

@Service
public class PhotoServiceImpl implements PhotoService {
    public static final String IMG_FOLDER_MITKO = "/Users/user/Desktop/Study/MyRepos/photocontest/Demo/src/main/resources/static/assets/img/";
    public static final String IMG_FOLDER_VANKATA = "/Users/ivele/Desktop/MyRepos/photocontest/Demo/src/main/resources/static/assets/img/";
    public static final String USER_PHOTO_ERROR_MESSAGE = "User cannot upload a photo.";

    private final PhotoRepository photoRepository;

    @Autowired
    public PhotoServiceImpl(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    @Override
    public List<Photo> getAll() {
        return photoRepository.getAll();
    }

    @Override
    public List<Photo> filter(PhotoFilterParameters filterParameters) {
        return photoRepository.filter(filterParameters);
    }

    @Override
    public Photo getById(int id) {
        return photoRepository.getById(id);
    }

    @Override
    public void create(Photo photo, User organizer) {
        checkAuthorization(organizer, PHOTO_JUNKIE_ROLE);
        if (!photo.getContest().getPhase().equals(PHASE_ONE)) {
            throw new UnsupportedOperationException(USER_PHOTO_ERROR_MESSAGE);
        }
        Contest contest = photo.getContest();
        List<Photo> photos = getAll();
        List<Photo> userPhotos = photos.stream().filter(photo1 -> photo1.getOwner().equals(organizer)).collect(Collectors.toList());
        List<Photo> collect = userPhotos.stream().filter(photo1 -> photo1.getContest().equals(contest)).collect(Collectors.toList());
        if (!collect.isEmpty()) {
            throw new DuplicateEntityException(USER_HAS_ALREADY_PHOTO_UPLOADED_ERROR_MESSAGE);
        }
        photoRepository.create(photo);
    }

    @Override
    public void update(Photo photo, User organizer) {
        checkAuthorization(organizer, ORGANIZER_ROLE);
        photoRepository.update(photo);
    }

    @Override
    public void delete(Photo photo, User organizer) {
        checkAuthorization(organizer, ORGANIZER_ROLE);
        photoRepository.delete(photo);
    }

    @Override
    public void saveImage(MultipartFile imageFile) throws IOException {
        String folder = IMG_FOLDER_VANKATA;
        byte[] bytes = imageFile.getBytes();
        Path path = Paths.get(folder + imageFile.getOriginalFilename());
        Files.write(path,bytes);
    }
}
