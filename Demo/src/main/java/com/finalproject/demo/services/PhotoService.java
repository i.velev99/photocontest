package com.finalproject.demo.services;

import com.finalproject.demo.models.Photo;
import com.finalproject.demo.models.User;
import com.finalproject.demo.models.filterParameters.PhotoFilterParameters;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PhotoService {
    List<Photo> getAll();

    List<Photo> filter(PhotoFilterParameters filterParameters);

    Photo getById(int id);

    void create(Photo photo, User user);

    void update(Photo photo, User organizer);

    void delete(Photo photo, User organizer);

    void saveImage(MultipartFile imageFile) throws IOException;
}
