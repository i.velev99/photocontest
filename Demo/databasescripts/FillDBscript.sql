insert into categories(name)
values ('City'),
       ('Nature'),
       ('Mountain');

insert into contest_types(name)
values ('Open'),
       ('Invitational');

insert into contests(title, category_id, type_id, start_date, first_phase_final_date, second_phase_final_date,cover_photo)
values ('The Night Life', 1, 1, '2021-04-21', '2021-05-21', '2021-05-22', 'https://images.unsplash.com/photo-1581535769770-7d83f98ce02b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80'),
       ('Best Outside Spot', 2, 1, '2021-03-20', '2021-04-21', '2021-04-26', 'https://images.unsplash.com/photo-1441974231531-c6227db76b6e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1951&q=80'),
       ('Wild Life', 2, 2, '2021-03-20', '2021-04-21', '2021-04-27','http://www.wildnatureinstitute.org/uploads/5/5/7/7/5577192/giraffestarangire_orig.jpg'),
       ('Mountain Shots', 3, 2, '2021-03-19', '2021-04-05', '2021-04-10','https://i.guim.co.uk/img/media/6088d89032f8673c3473567a91157080840a7bb8/413_955_2808_1685/master/2808.jpg?width=1200&height=1200&quality=85&auto=format&fit=crop&s=412cc526a799b2d3fff991129cb8f030');

insert into user_credentials(username, password)
values ('gosho', '12345678'),
       ('toni', '12345678'),
       ('dimitar', '12345678'),
       ('pesho', '12345678'),
       ('vankata', '12345678'),
       ('ivanX', '12345678');

insert into users(first_name, last_name, credentials_id, points)
values ('Georgi', 'Ivanov', 'gosho', 1200),
       ('Anton', 'Donev', 'toni', 600),
       ('Dimitar', 'Iliev', 'dimitar', 435),
       ('Petar', 'Georgiev', 'pesho', 15),
       ('Ivan', 'Iliev', 'vankata', 30),
       ('Ivan', 'Xander', 'ivanX', 75);


insert into roles(name)
values ('Photo Junkie'),
       ('Organizer');

insert into users_roles(credentials_id, role_id)
values ('gosho', 2),
       ('toni', 1),
       ('dimitar', 1),
       ('pesho', 1),
       ('vankata', 1),
       ('ivanX',1);

insert into contest_juries(contest_id, user_id)
values (1, 1),
       (1, 2),
       (1, 3),
       (2, 1),
       (2, 2),
       (2, 3),
       (3, 1),
       (3, 2),
       (3, 3),
       (4, 1),
       (4, 2);

insert into photos(title, story, contest_id, owner_id, upload_date, photo, score)
values ('Disco in Belgrade', 'Belgrade at night, is awesome', 1, 4, '2021-01-03', '~/assets/img/belgrade-disco.jpg', 7.3),
       ('Sofia Lulin at night', 'It is a busy street, but looks good ', 1, 5, '2021-01-03', '~/assets/img/outside.jpeg', 8.2),
       ('Tokyo at night', 'I visited my grandma last year in Tokyo and took that photo', 1, 6, '2021-01-03', '~/assets/img/tokyo.jpg', 8.5),
       ('Garden', 'My grandmas backyard, its amazing', 2, 4, '2021-02-10', '~/assets/img/grandma-garden.jpg', 6.3),
       ('Secret Lake', 'Only locals in my village know about that place', 2, 5, '2021-02-10', '~/assets/img/secret-lake.jpg', 9.5),
       ('New York Central Park lake', 'It is a beautiful lake near the center of New York, I enjoy going there every day ', 2, 6, '2021-02-10', '~/assets/img/new-york-center.jpg', 9.2),
       ('African Wild Life  ', 'Shoot it last year, when we went on a trip to Africa ', 3, 4, '2021-02-8', '~/assets/img/animals.jpg', 6.2),
       ('Roaring Lioness', 'A caught on shot, took me about to weeks to catch right', 3, 5, '2021-02-8', '~/assets/img/lioness.jpg', 8.6),
       ('Lion Family', 'A Lion family, I shoot last, when I went on a trip to Africa ', 3, 6, '2021-02-8', '~/assets/img/lion-family.jpg', 8.6),
       ('The Seven Rila Lakes', 'Shoot it last year, when we went on a trip to Bulgaria, It was the most beautiful mountain I have seen', 4, 4, '2021-02-8', '~/assets/img/seven-rila-lakes.jpeg', 8.2),
       ('Himalayan mountain', 'Amazing spot, if you are into climbing', 4, 5, '2021-02-8', '~/assets/img/himalayan.jpg', 6.6),
       ('Sinanica', 'Best place for hiking and enjoing the nature around you', 4, 6, '2021-02-8', '~/assets/img/sinanica.jpg', 8.6);

insert into reviews(points, review, created_by,photo_id)
values (8, 'Caught on picture', 1 , 4),
       (4, 'It is not very professional photo', 2 , 4),
       (0, 'Not appropriate', 1 , 5),
       (5, 'kinda good', 2 , 5),
       (10, 'One of the best I have seen', 1, 6),
       (8, 'Very good Picture', 2, 6),
       (9, 'Spectucalor Photo', 1, 7),
       (8, 'Amazing Photo', 2, 7),
       (2, 'It did not caught my eye', 1, 8),
       (9, 'Very caught on', 2, 8),
       (7, 'Nice shot', 1, 9),
       (8, 'Good eye for a picture', 2, 9),
       (10, 'Good shot at an amazing place', 1, 10),
       (9, 'Very nice place, with good quality and lighting', 2, 10),
       (4, 'Good spot, but I dont really like the shadows', 1, 11),
       (3, 'Bad quality, but the spot is decent', 2, 11),
       (8, 'Amazing place, with good quality', 1, 12),
       (10, 'I wish I was there, the place is amazing', 2 ,12);

insert into contest_participants(contest_id, user_id)
VALUES (1,4),
       (1,5),
       (1,6),
       (2,4),
       (2,5),
       (2,6),
       (3,4),
       (3,5),
       (3,6),
       (4,4),
       (4,5),
       (4,6);