create table categories
(
    id   int auto_increment
        primary key,
    name varchar(50) not null
);

create table contest_types
(
    id   int auto_increment
        primary key,
    name varchar(50) not null
);

create table contests
(
    id                      int auto_increment
        primary key,
    title                   varchar(50)       not null,
    category_id             int               not null,
    type_id                 int               not null,
    start_date              date              null,
    first_phase_final_date  date              null,
    second_phase_final_date date              null,
    is_scored               tinyint default 0 not null,
    cover_photo             varchar(1000)     not null,

    constraint contests_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint contests_contest_types_id_fk
        foreign key (type_id) references contest_types (id)
);

create table roles
(
    id   int auto_increment
        primary key,
    name varchar(50) not null
);

create table user_credentials
(
    username varchar(45) not null
        primary key,
    password varchar(50) not null
);

create table users
(
    id             int auto_increment
        primary key,
    first_name     varchar(20) not null,
    last_name      varchar(20) not null,
    credentials_id varchar(50) not null,
    points         int         not null,
    constraint users_user_credentials_username_fk
        foreign key (credentials_id) references user_credentials (username)
);

create table contest_juries
(
    contest_id int not null,
    user_id    int not null,
    constraint contest_juries_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint contest_juries_users_id_fk
        foreign key (user_id) references users (id)
);

create table contest_participants
(
    contest_id int not null,
    user_id    int not null,
    constraint contest_participants_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint contest_participants_users_id_fk
        foreign key (user_id) references users (id)
);

create table photos
(
    id          int auto_increment
        primary key,
    title       varchar(50)   not null,
    story       varchar(1000) not null,
    contest_id  int           not null,
    owner_id    int           not null,
    upload_date date          not null,
    photo       varchar(1000) not null,
    place       int default 0 not null,
    score       double        not null,
    constraint photos_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint photos_users_id_fk
        foreign key (owner_id) references users (id)
);

create table reviews
(
    id         int auto_increment
        primary key,
    points     int           not null,
    review     varchar(1000) not null,
    created_by int           not null,
    photo_id   int           not null,
    constraint reviews_users_id_fk
        foreign key (created_by) references users (id),
    constraint reviews_photos_id_fk
        foreign key (photo_id) references photos (id)
);

create table users_roles
(
    credentials_id varchar(50) not null,
    role_id        int         not null,
    constraint users_roles_credentials_id_fk
        foreign key (credentials_id) references user_credentials (username),
    constraint users_roles_roles_id_fk
        foreign key (role_id) references roles (id)
);

